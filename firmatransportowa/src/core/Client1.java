package core;

import java.math.BigDecimal;
import java.sql.Date;


public class Client1 {

	private int Id;
	private String ForeName;
	private String LastName;

	private long Pesel;
	private long Nip;
	private int LocationId;	
	private int PhoneNumber;
	private String Mail;


	
	public Client1(int Id,String ForeName,String LastName,long Pesel,long Nip,int LocationId,int PhoneNumber,String Mail) {
		super();
		this.Id= Id;
		this.ForeName = ForeName;
		this.LastName = LastName;
		this.Pesel = Pesel;
		this.Nip = Nip;
		this.LocationId = LocationId;
		
		this.PhoneNumber = PhoneNumber;
		this.Mail = Mail;
		
	}


public int getId(){
	return Id;
}
public void setId(int id){
	Id=id;
}
public String getForeName() {
	return ForeName;
}public void setName(String Forename) {
	ForeName = Forename;
}public String getLastName() {
	return LastName;
}public void setLastName(String lastName) {
	LastName = lastName;
}public int getLocationId() {
	return LocationId;
}public void setLocationId(int locationId) {
	LocationId = locationId;
}public long getNip() {
	return Nip;
}public void setNip(long nip) {
	Nip = nip;
}public long getPesel() {
	return Pesel;
}public void setPesel(long pesel) {
	Pesel = pesel;
}

public int getPhoneNumber() {
	return PhoneNumber;
}

public void setPhoneNumber(int PhoneNumber) {
	this.PhoneNumber = PhoneNumber;
}


	public String getMail() {
		return Mail;
	}

	public void setMail(String Mail) {
		this.Mail = Mail;
	}

	



	@Override
	public String toString() {
		return String
				.format("Client1 [Id=%s, ForeName=%s,LastName=%s, Pesel=%s, Nip=%s, LocationId=%s, PhoneNumber=%s, Mail=%s]",
						Id,ForeName,LastName, Pesel, Nip, LocationId, PhoneNumber, Mail);
	}
	
	
		
}
