package core;

import java.math.BigDecimal;
import java.sql.Date;


public class Employee {

	private int Id;
	private int JobsId;
	private String firstName;
	private String lastName;
	private long Pesel;
	private Date BirthDate;
	private BigDecimal MonthSalary;
	private Date EmploymentDate;
	private String Mail;
	private int PhoneNumber;


	
	public Employee(int Id, int JobsId,String firstName,String lastName,long Pesel,Date BirthDate,
			BigDecimal MonthSalary,Date EmploymentDate,String Mail,int PhoneNumber) {
		super();
	    this.Id = Id;
		this.JobsId = JobsId;
		this.firstName = firstName;
		this.lastName = lastName;
		this.Pesel = Pesel;
		this.BirthDate = BirthDate;
		this.MonthSalary = MonthSalary;
		this.EmploymentDate = EmploymentDate;
		this.Mail = Mail;
		this.PhoneNumber = PhoneNumber;
		
	}


	public int getId(){
		return Id;
	}
	public void setId(int Id) {
		this.Id = Id;
	}

	public int getJobsId() {
		return JobsId;
	}
	public void setJobsId(int JobsId) {
		this.JobsId = JobsId;
	}

	

	public String getFirstName() {
		return firstName;
	}

	public void setFirstName(String firstName) {
		this.firstName = firstName;
	}
	public String getLastName() {
		return lastName;
	}
	public void setLastName(String lastName) {
		this.lastName = lastName;
	}
	
	public long getPesel() {
		return Pesel;
	}

	public void setPesel(long Pesel) {
		this.Pesel = Pesel;
	}
	public Date getBirthDate(){
		return BirthDate;
	}
	public void setBirthDate(Date BirthDate){
		this.BirthDate = BirthDate;
	}
	public BigDecimal getMonthSalary() {
		return MonthSalary;
	}
	public void setMonthSalary(BigDecimal MonthSalary) {
		this.MonthSalary = MonthSalary;
	}
	public Date getEmploymentDate(){
		return EmploymentDate;
	}
	public void setEmploymentDate(Date EmploymentDate){
		this.EmploymentDate = EmploymentDate;
	}

	public String getMail() {
		return Mail;
	}

	public void setEmail(String Mail) {
		this.Mail = Mail;
	}

	

	public int getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(int PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
	}

	@Override
	public String toString() {
		return String
				.format("Employee [Id=%s, JobsId=%s, firstName=%s, lastName=%s, Pesel=%s, BirthDate=%s,MonthSalary=%s ,EmploymentDate=%s ,Mail=%s"
						+ " , PhoneNumber=%s]",
						 Id, JobsId, firstName, lastName, Pesel, BirthDate,MonthSalary ,EmploymentDate ,Mail , PhoneNumber);
	}
	
	
		
}
