package core;

import java.math.BigDecimal;
import java.sql.Date;


public class Client {

	private int Id;
	private String Name;
	private long Nip;
	private long Regon;
	private int LocationId;
	private String Mail;
	private int PhoneNumber;


	
	public Client(int Id,String Name,long Nip,long Regon,int LocationId,String Mail,int PhoneNumber) {
		super();
		this.Id = Id;
		this.Name = Name;
		this.Nip = Nip;
		this.Regon = Regon;
		this.LocationId = LocationId;
		this.Mail = Mail;
		this.PhoneNumber = PhoneNumber;
		
		
	}


public int getId(){
	return Id;
}
public void setId(int id){
	Id = id;
}
public String getName() {
	return Name;
}public void setName(String name) {
	Name = name;
}public int getLocationId() {
	return LocationId;
}public void setLocationId(int locationId) {
	LocationId = locationId;
}public long getNip() {
	return Nip;
}public long getRegon() {
	return Regon;
}public void setNip(long nip) {
	Nip = nip;
}public void setRegon(long regon) {
	Regon = regon;
}

	


	public String getMail() {
		return Mail;
	}

	public void setmail(String Mail) {
		this.Mail = Mail;
	}

	

	public int getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(int PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
	}

	@Override
	public String toString() {
		return String
				.format("Client [Id=%s,Name=%s, Nip=%s, Regon=%s, LocationId=%s, Mail=%s, PhoneNumber=%s]",
					Id, Name, Nip, Regon, LocationId, Mail, PhoneNumber);
	}
	
	
		
}
