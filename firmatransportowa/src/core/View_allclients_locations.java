package core;

import java.math.BigDecimal;


public class View_allclients_locations {

	private int id;
	private String Company_LastName;
	private String ForeName;
	private String Mail;
	private long Pesel_Regon;
	private long NIP;
	private int PhoneNumber;
	private String StreetAddressLine1;
	private String StreetAddressLine2;
	private String PostalCode;
	private String CityName;
	private String CountryName;
	private boolean IfLegalEntity;
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
	
	public View_allclients_locations(int id, String Company_LastName, String ForeName, long Pesel_Regon, long NIP, String Mail, 
			int PhoneNumber, String StreetAddressLine1, String StreetAddressLine2,  String PostalCode, String CityName, 
			String CountryName, boolean IfLegalEntity)
	{
		super();
		this.id = id;
		this.Company_LastName = Company_LastName;
		this.ForeName = ForeName;
		this.Pesel_Regon = Pesel_Regon;
		this.NIP = NIP;
		this.Mail = Mail;
		this.PhoneNumber = PhoneNumber;
		this.StreetAddressLine1 = StreetAddressLine1;
		this.StreetAddressLine2 = StreetAddressLine2;
		this.PostalCode = PostalCode;
		this.CityName = CityName;
		this.CountryName = CountryName;
		this.IfLegalEntity = IfLegalEntity;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getCompany_LastName() {
		return Company_LastName;
	}

	public void setCompany_LastName(String Company_LastName) {
		this.Company_LastName = Company_LastName;
	}

	public String getForeName() {
		return ForeName;
	}

	public void setForeName(String ForeName) {
		this.ForeName = ForeName;
	}
	
	public long getPesel_Regon() {
		return Pesel_Regon;
	}

	public void setPesel_Regon(long Pesel_Regon) {
		this.Pesel_Regon = Pesel_Regon;
	}
	
	public long getNIP() {
		return NIP;
	}

	public void setNIP(long NIP) {
		this.NIP = NIP;
	}
	
	public String getMail() {
		return Mail;
	}

	public void setmail(String Mail) {
		this.Mail = Mail;
	}
	
	public int getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(int PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
	}
	public String getStreetAddressLine1() {
		return StreetAddressLine1;
	}

	public void setStreetAddressLine1(String StreetAddressLine1) {
		this.StreetAddressLine1 = StreetAddressLine1;
	}
	public String getStreetAddressLine2() {
		return StreetAddressLine2;
	}

	public void setStreetAddressLine2(String StreetAddressLine2) {
		this.StreetAddressLine2 = StreetAddressLine2;
	}
	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String PostalCode) {
		this.PostalCode = PostalCode;
	}
	public String getCityName() {
		return CityName;
	}

	public void setCityName(String CityName) {
		this.CityName = CityName;
	}
	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String CountryName) {
		this.CountryName = CountryName;
	}
	
	public boolean getIfLegalEntity() {
		return IfLegalEntity;
	}

	public void setIfLegalEntity(boolean IfLegalEntity) {
		this.IfLegalEntity = IfLegalEntity;
	}

	

	@Override
	public String toString() {
		return String
				.format("Employee [id=%s, ForeName=%s, Company_LastName=%s, Pesel_Regon=%s, NIP=%s , Mail=%s, PhoneNumber=%s, StreetAddressLine1=%s, StreetAddressLine2=%s, PostalCode=%s, CityName=%s, CountryName=%s, IfLegalEntity=%s]",
						id, ForeName, Company_LastName,
						Pesel_Regon, NIP, Mail, PhoneNumber, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName, IfLegalEntity);
	}
	
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

		
}

