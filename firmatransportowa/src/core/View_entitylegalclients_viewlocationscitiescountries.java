 package core;

import java.math.BigDecimal;


public class View_entitylegalclients_viewlocationscitiescountries {

	private int id;
	private String Name;
	private long NIP;
	private long Regon;
	private String Mail;
	private int PhoneNumber;
	private String StreetAddressLine1;
	private String StreetAddressLine2;
	private int PostalCode;
	private String CityName;
	private String CountryName;
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
	
	public View_entitylegalclients_viewlocationscitiescountries(int id, String Name, long NIP,long Regon, String Mail,
	 int PhoneNumber, String StreetAddressLine1, String StreetAddressLine2, int PostalCode, String CityName, String CountryName)
	{
		super();
		this.id = id;
		this.Name = Name;
		this.NIP = NIP;
		this.Regon = Regon;
		this.Mail = Mail;
		this.PhoneNumber = PhoneNumber;
		this.StreetAddressLine1 = StreetAddressLine1;
		this.StreetAddressLine2 = StreetAddressLine2;
		this.PostalCode = PostalCode;
		this.CityName = CityName;
		this.CountryName = CountryName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getName() {
		return Name;
	}

	public void setName(String Name) {
		this.Name = Name;
	}


	public long getRegon() {
		return Regon;
	}

	public void setPesel_Regon(long Regon) {
		this.Regon = Regon;
	}
	
	public long getNIP() {
		return NIP;
	}

	public void setNIP(long NIP) {
		this.NIP = NIP;
	}
	
	public String getMail() {
		return Mail;
	}

	public void setmail(String Mail) {
		this.Mail = Mail;
	}
	
	public int getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(int PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
	}
	public String getStreetAddressLine1() {
		return StreetAddressLine1;
	}

	public void setStreetAddressLine1(String StreetAddressLine1) {
		this.StreetAddressLine1 = StreetAddressLine1;
	}
	public String getStreetAddressLine2() {
		return StreetAddressLine2;
	}

	public void setStreetAddressLine2(String StreetAddressLine2) {
		this.StreetAddressLine2 = StreetAddressLine2;
	}
	public int getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(int PostalCode) {
		this.PostalCode = PostalCode;
	}
	public String getCityName() {
		return CityName;
	}

	public void setCityName(String CityName) {
		this.CityName = CityName;
	}
	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String CountryName) {
		this.CountryName = CountryName;
	}
	

	

	@Override
	public String toString() {
		return String
				.format("Employee [id=%s, Name=%s, Regon=%s, NIP=%s , Mail=%s, PhoneNumber=%s, StreetAddressLine1=%s, StreetAddressLine2=%s, PostalCode=%s, CityName=%s, CountryName=%s]",
						id, Name, 
						Regon, NIP, Mail, PhoneNumber, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName );
	}
	
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

		
}

