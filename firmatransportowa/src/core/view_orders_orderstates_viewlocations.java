
	package core;

	import java.math.BigDecimal;
import java.sql.Date;


	public class view_orders_orderstates_viewlocations {

		private int Id;
		private String State;
		private	boolean IfLegalEntity;
		private String ClientId;
		private Date Deadline;
		private Date DepartureDate;
		private String Description;
		private BigDecimal Price;
		private String Street_Dest;
		private String Address_Dest;
		private String Code_Dest;
		private String City_Dest;
		private String Country_Dest;
		private String Street_Src;
		private String Address_Src;
		private String Code_Src;
		private String City_Src;
		private String Country_Src;
		
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
		
		public view_orders_orderstates_viewlocations(int Id,
		String State,
		boolean IfLegalEntity,
		 String ClientId,
		 Date Deadline,
		 Date DepartureDate,
		 String Description,
		 BigDecimal Price,
		 String Street_Dest,
		 String Address_Dest,
		 String Code_Dest,
		 String City_Dest,
		 String Country_Dest,
		 String Street_Src,
		 String Address_Src,
		 String Code_Src,
		 String City_Src,
		 String Country_Src)
		{
			super();
			this.Id = Id;
			this.State = State;
			this.IfLegalEntity = IfLegalEntity;
			this.ClientId = ClientId;
			this.Deadline = Deadline;
			this.DepartureDate = DepartureDate;
			this.Description = Description;
			this.Price = Price;
			this.Street_Dest = Street_Dest;
			this.Address_Dest = Address_Dest;
			this.Code_Dest = Code_Dest;
			this.City_Dest = City_Dest;
			this.Country_Dest = Country_Dest;
			this.Street_Src = Street_Src;
			this.Address_Src = Address_Src;
			this.Code_Src = Code_Src;
			this.City_Src = City_Src;
			this.Country_Src = Country_Src;
		}

		public int getId() {
			return Id;
		}

		public void setId(int Id) {
			this.Id = Id;
		}


		public String getState() {
			return State;
		}

		public void setState(String State) {
			this.State = State;
		}
		public boolean getIfLegalEntity(){
			return IfLegalEntity;
		}
		public void setIfLegalEntity(boolean IfLegalEntity){
			this.IfLegalEntity = IfLegalEntity;
		}
		
		public String getClientId() {
			return ClientId;
		}

		public void setClientId(String ClientId) {
			this.ClientId = ClientId;
		}
		
		public String getDescription() {
			return Description;
		}

		public void setDescription(String Description) {
			this.Description = Description;
		}
		public Date getDeadline() {
			return Deadline;
		}

		public void setDeadline(Date Deadline) {
			this.Deadline = Deadline;
		}

		public BigDecimal getPrice() {
			return Price;
		}

		public void setPrice(BigDecimal Price) {
			this.Price = Price;
		}
		public Date getDepartureDate() {
			return DepartureDate;
		}

		public void setDepartureDate(Date DepartureDate) {
			this.DepartureDate = DepartureDate;
		}
		public String getStreet_Dest() {
			return Street_Dest;
		}

		public void setStreet_Dest(String Street_Dest) {
			this.Street_Dest = Street_Dest;
		}
		public String getAddress_Dest() {
			return Description;
		}

		public void setAddress_Dest(String Address_Dest) {
			this.Address_Dest = Address_Dest;
		}
		public String getCode_Dest() {
			return Code_Dest;
		}

		public void setCode_Dest(String Code_Dest) {
			this.Code_Dest = Code_Dest;
		}
		public String getCity_Dest() {
			return City_Dest;
		}

		public void setCity_Dest(String City_Dest) {
			this.City_Dest = City_Dest;
		}
		public String getCountry_Dest() {
			return Country_Dest;
		}

		public void setCountry_Dest(String Country_Dest) {
			this.Country_Dest = Country_Dest;
		}
		public String getStreet_Src() {
			return Street_Src;
		}

		public void setStreet_Src(String Street_Src) {
			this.Street_Src = Street_Src;
		}
		public String getAddress_Src() {
			return Address_Src;
		}

		public void setAddress_Src(String Address_Src) {
			this.Address_Src = Address_Src;
		}
		public String getCode_Src() {
			return Code_Src;
		}

		public void setCode_Src(String Code_Src) {
			this.Code_Src = Code_Src;
		}
		public String getCity_Src() {
			return Description;
		}

		public void setCity_Src(String City_Src) {
			this.City_Src = City_Src;
		}
		public String getCountry_Src() {
			return Country_Src;
		}

		public void setCountry_Src(String Country_Src) {
			this.Country_Src = Country_Src;
		}


		@Override
		public String toString() {
			return String
					.format("Employee [Id=%s, State=%s, IfLegalEntity=%s, ClientId=%s, Deadline=%s"
							+ " , DepartureDate=%s,Description=%s, Price=%s, Street_Dest=%s, Address_Dest=%s, "
							+ "Code_Dest=%s, City_Dest=%s, Country_Dest=%s, Street_Src=%s, "
							+ "Address_Src=%s, Code_Src=%s, City_Src=%s, "
							+ "Country_Src=%s ]",
							Id,
							 State,
							 IfLegalEntity,
							  ClientId,
							  Deadline,
							  DepartureDate,
							  Description,
							  Price,
							  Street_Dest,
							  Address_Dest,
							  Code_Dest,
							  City_Dest,
							  Country_Dest,
							  Street_Src,
							  Address_Src,
							  Code_Src,
							  City_Src,
							  Country_Src);
		}
		
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

			
	}

