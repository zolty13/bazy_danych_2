
	package core;

	import java.math.BigDecimal;


	public class View_naturalpersonclients_viewlocationscitiescountries {

		private int id;
		private String LastName;
		private String ForeName;
		private String Mail;
		private long Pesel;
		private long NIP;
		private int PhoneNumber;
		private String StreetAddressLine1;
		private String StreetAddressLine2;
		private String PostalCode;
		private String CityName;
		private String CountryName;
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
		
		public View_naturalpersonclients_viewlocationscitiescountries(int id, String ForeName,  String LastName,long Pesel, long NIP, String Mail, 
				int PhoneNumber, String StreetAddressLine1, String StreetAddressLine2,  String PostalCode, String CityName, 
				String CountryName)
		{
			super();
			this.id = id;
			this.ForeName = ForeName;
			this.LastName = LastName;
			this.Pesel = Pesel;
			this.NIP = NIP;
			this.Mail = Mail;
			this.PhoneNumber = PhoneNumber;
			this.StreetAddressLine1 = StreetAddressLine1;
			this.StreetAddressLine2 = StreetAddressLine2;
			this.PostalCode = PostalCode;
			this.CityName = CityName;
			this.CountryName = CountryName;
		}

		public int getId() {
			return id;
		}

		public void setId(int id) {
			this.id = id;
		}


		public String getForeName() {
			return ForeName;
		}

		public void setForeName(String ForeName) {
			this.ForeName = ForeName;
		}
		public String getLastName() {
			return LastName;
		}

		public void setLastName(String LastName) {
			this.LastName = LastName;
		}
		
		public long getPesel() {
			return Pesel;
		}

		public void setPesel_Pesel(long Pesel_Pesel) {
			this.Pesel = Pesel_Pesel;
		}
		
		public long getNIP() {
			return NIP;
		}

		public void setNIP(long NIP) {
			this.NIP = NIP;
		}
		
		public String getMail() {
			return Mail;
		}

		public void setmail(String Mail) {
			this.Mail = Mail;
		}
		
		public int getPhoneNumber() {
			return PhoneNumber;
		}

		public void setPhoneNumber(int PhoneNumber) {
			this.PhoneNumber = PhoneNumber;
		}
		public String getStreetAddressLine1() {
			return StreetAddressLine1;
		}

		public void setStreetAddressLine1(String StreetAddressLine1) {
			this.StreetAddressLine1 = StreetAddressLine1;
		}
		public String getStreetAddressLine2() {
			return StreetAddressLine2;
		}

		public void setStreetAddressLine2(String StreetAddressLine2) {
			this.StreetAddressLine2 = StreetAddressLine2;
		}
		public String getPostalCode() {
			return PostalCode;
		}

		public void setPostalCode(String PostalCode) {
			this.PostalCode = PostalCode;
		}
		public String getCityName() {
			return CityName;
		}

		public void setCityName(String CityName) {
			this.CityName = CityName;
		}
		public String getCountryName() {
			return CountryName;
		}

		public void setCountryName(String CountryName) {
			this.CountryName = CountryName;
		}
		

		

		@Override
		public String toString() {
			return String
					.format("Employee [id=%s, ForeName=%s, LastName=%s, Pesel=%s, NIP=%s , Mail=%s, PhoneNumber=%s, StreetAddressLine1=%s, StreetAddressLine2=%s, PostalCode=%s, CityName=%s, CountryName=%s]",
							id, ForeName, LastName,
							Pesel, NIP, Mail, PhoneNumber, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName);
		}
		
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

			
	}

