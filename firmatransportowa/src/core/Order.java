package core;

import java.math.BigDecimal;
import java.sql.Date;


public class Order {
	
	private int Id;
	private boolean IfLegalEntity;
	private int ClientId;
	private int OrderStateId;
	private int DestinationLocationId;
	private int SourceLocationId;
	private String Description;
	private Date Deadline;
	private BigDecimal Price;
	private Date DepartureDate;
	

	
	public Order(int Id,boolean IfLegalEntity,int ClientId,int OrderStateId,int DestinationLocationId,
			int SourceLocationId,String Description,Date Deadline,BigDecimal Price,Date DepartureDate){
		super();
		this.Id=Id;
		this.IfLegalEntity = IfLegalEntity;
		this.ClientId = ClientId;
		this.OrderStateId = OrderStateId;
		this.DestinationLocationId = DestinationLocationId;
		this.SourceLocationId = SourceLocationId;
		this.Description = Description;
		this.Deadline = Deadline;
		this.Price = Price;
		this.DepartureDate = DepartureDate;
		
	}
//	private int I;
//	private int Id;
//	private boolean IfLegalEntity;
//	private int ClientId;
//	private int OrderStateId;
//	private int DestinationLocationId;
//	private int SourceLocationId;
//	private String Description;
//	private Date Deadline;
//	private BigDecimal Price;
//	private Date DepartureDate;

	public int getId() {
		return Id;
	}

	public void setId(int Id) {
		this.Id = Id;
	}

	 public boolean getIfLegalEntity() {
		return IfLegalEntity;
	}
	 public void setIfLegalEntity(boolean ifLegalEntity) {
		IfLegalEntity = ifLegalEntity;
	}
	 public int getClientId() {
		return ClientId;
	}
	 public void setClientId(int clientId) {
		ClientId = clientId;
	}
	 public int getOrderStateId() {
		return OrderStateId;
	}
	 public void setOrderStateId(int orderStateId) {
		OrderStateId = orderStateId;
	}
	 public int getDestinationLocationId() {
		return DestinationLocationId;
	}
	 public void setDestinationLocationId(int destinationLocationId) {
		DestinationLocationId = destinationLocationId;
	}
	 public String getDescription() {
		return Description;
	}public void setDescription(String description) {
		Description = description;
	}public int getSourceLocationId() {
		return SourceLocationId;
	}public void setSourceLocationId(int sourceLocationId) {
		SourceLocationId = sourceLocationId;
	}public Date getDeadline() {
		return Deadline;
	}public void setDeadline(Date deadline) {
		Deadline = deadline;
	}public Date getDepartureDate() {
		return DepartureDate;
	}public void setDepartureDate(Date departureDate) {
		DepartureDate = departureDate;
	}public BigDecimal getPrice() {
		return Price;
	}public void setPrice(BigDecimal price) {
		Price = price;
	}

	@Override
	public String toString() {
		return String
				.format("Order [Id=%s, IfLegalEntity=%s, ClientId=%s, OrderStateId=%s, DestinationLocationId=%s,SourceLocationId=%s ,Description=%s ,Deadline=%s"
						+ " , Price=%s, DepartureDate=%s]",
						 Id, IfLegalEntity, ClientId, OrderStateId, DestinationLocationId,
						 SourceLocationId, Description, Deadline, Price, DepartureDate);
	}
	
	
		
}
