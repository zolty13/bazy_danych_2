

	package core;

	import java.math.BigDecimal;
import java.sql.Date;


	public class View_orders_employees_clients {

		private int Orderid;
		private String State;
		private	boolean IfLegalEntity;
		private String ClientId;
		private String ForeName;
		private String Company_LastName;
		private long Pesel_Regon;
		private String Cmail;
		private int CPhone;
		private int DestinationLocationID;
		private int SourceLocationID;
		private String Description;
		private Date Deadline;
		private BigDecimal Price;
		private Date DepartureDate;
		private int EmpId;
		private String FirstName;
		private String LastName;
		private long Pesel;
		private String Mail;
		private int PhoneNumber;
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
		
		public View_orders_employees_clients(int Orderid, String State,boolean IfLegalEntity, String ClientId,String ForeName,String Company_LastName, 
				long Pesel_Regon, String Cmail, int CPhone, int DestinationLocationID, int SourceLocationID, String Description, Date Deadline, 
				BigDecimal Price, Date DepartureDate,int EmpId, String FirstName,  String LastName, long Pesel, 
				String Mail, int PhoneNumber)
		{
			super();
			this.Orderid = Orderid;
			this.State = State;
			this.IfLegalEntity = IfLegalEntity;
			this.ClientId = ClientId;
			this.ForeName = ForeName;
			this.Company_LastName = Company_LastName;
			this.Pesel_Regon = Pesel_Regon;
			this.Cmail = Cmail;
			this.CPhone = CPhone;
			this.DestinationLocationID = DestinationLocationID;
			this.SourceLocationID = SourceLocationID;
			this.Description = Description;
			this.Deadline = Deadline;
			this.Price = Price;
			this.DepartureDate = DepartureDate;
			this.EmpId = EmpId;
			this.FirstName = FirstName;
			this.PhoneNumber = PhoneNumber;
		}

		public int getOrderid() {
			return Orderid;
		}

		public void setOrderid(int Orderid) {
			this.Orderid = Orderid;
		}


		public String getState() {
			return State;
		}

		public void setState(String State) {
			this.State = State;
		}
		public boolean getIfLegalEntity(){
			return IfLegalEntity;
		}
		public void setIfLegalEntity(boolean IfLegalEntity){
			this.IfLegalEntity = IfLegalEntity;
		}
		
		public String getClientId() {
			return ClientId;
		}

		public void setClientId(String ClientId) {
			this.ClientId = ClientId;
		}
		public String getForeName() {
			return ForeName;
		}

		public void setForeName(String ForeName) {
			this.ForeName = ForeName;
		}
		public String getCompany_LastName() {
			return Company_LastName;
		}

		public void setCompany_LastName(String Company_LastName) {
			this.Company_LastName = Company_LastName;
		}
		
		
		
		
		public long getPesel_Regon() {
			return Pesel_Regon;
		}

		public void setPesel_Pesel(long Pesel_Regon) {
			this.Pesel_Regon = Pesel_Regon;
		}
		
		public String getCmail() {
			return Cmail;
		}

		public void setCmail(String Cmail) {
			this.Cmail = Cmail;
		}
		public int getCPhone() {
			return CPhone;
		}

		public void setCPhone(int CPhone) {
			this.CPhone = CPhone;
		}


		public int getDestinationLocationID() {
			return DestinationLocationID;
		}

		public void setDestinationLocationID(int DestinationLocationID) {
			this.DestinationLocationID = DestinationLocationID;
		}
		public int getSourceLocationID() {
			return SourceLocationID;
		}

		public void setSourceLocationID(int SourceLocationID) {
			this.SourceLocationID = SourceLocationID;
		}
		public String getDescription() {
			return Description;
		}

		public void setDescription(String Description) {
			this.Description = Description;
		}
		public Date getDeadline() {
			return Deadline;
		}

		public void setDeadline(Date Deadline) {
			this.Deadline = Deadline;
		}

		public BigDecimal getPrice() {
			return Price;
		}

		public void setPrice(BigDecimal Price) {
			this.Price = Price;
		}
		public Date getDepartureDate() {
			return DepartureDate;
		}

		public void setDepartureDate(Date DepartureDate) {
			this.DepartureDate = DepartureDate;
		}
		public int getEmpId() {
			return EmpId;
		}

		public void setEmpId(int EmpId) {
			this.EmpId = EmpId;
		}
		public String getFirstName() {
			return FirstName;
		}

		public void setFirstName(String FirstName) {
			this.FirstName = FirstName;
		}
		public String getLastName() {
			return LastName;
		}

		public void setLastName(String LastName) {
			this.LastName = LastName;
		}
		public long getPesel() {
			return Pesel;
		}

		public void setPesel(long Pesel) {
			this.Pesel = Pesel;
		}
		public String getMail() {
			return Mail;
		}

		public void setMail(String Mail) {
			this.Mail = Mail;
		}
		public int getPhoneNumber() {
			return PhoneNumber;
		}

		public void setPhoneNumber(int PhoneNumber) {
			this.PhoneNumber = PhoneNumber;
		}
//		private int Orderid;
//		private String State;
//		private	boolean IfLegalEntity;
//		private String ClientId;
//		private String ForeName;
//		private String Company_LastName;
//		private BigDecimal Pesel_Regon;
//		private String Cmail;
//		private int CPhone;
//		private int DestinationLocationID;
//		private int SourceLocationID;
//		private String Description;
//		private Date Deadline;
//		private BigDecimal Price;
//		private Date DepartureDate;
//		private int EmpId;
//		private String FirstName;
//		private String LastName;
//		private BigDecimal Pesel;
//		private String Mail;
//		private int PhoneNumber;

		@Override
		public String toString() {
			return String
					.format("Employee [Orderid=%s, State=%s, IfLegalEntity=%s, ClientId=%s, ForeName=%s"
							+ " , Company_LastName=%s,Pesel_Regon=%s, Cmail=%s, CPhone=%s, DestinationLocationID=%s, "
							+ "SourceLocationID=%s, Description=%s, Deadline=%s, Price=%s, "
							+ "DepartureDate=%s, EmpId=%s, FirstName=%s, "
							+ "LastName=%s, Pesel=%s, Mail=%s,PhoneNumber=%s ]",
							Orderid, State, IfLegalEntity, ClientId, ForeName
									, Company_LastName,Pesel_Regon, Cmail, CPhone, DestinationLocationID,
									SourceLocationID, Description, Deadline, Price, 
									DepartureDate, EmpId, FirstName, 
									LastName, Pesel, Mail,PhoneNumber);
		}
		
		//"Id", "ForeName", "Company_LastName",
		//"Pesel_Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

			
	}

