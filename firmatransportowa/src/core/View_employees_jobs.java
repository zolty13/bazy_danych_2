package core;

import java.math.BigDecimal;
import java.sql.Date;

public class View_employees_jobs {

	private int id;
	private String JobName;
	private String FirstName;
	private String LastName;
	private long Pesel;
	private String Mail;
	private int PhoneNumber;
	private Date BirthDate;
	private BigDecimal MonthSalary;
	private Date EmploymentDate;

	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
	
	public View_employees_jobs(int id, String JobName, String FirstName, String LastName,long Pesel, String Mail,int PhoneNumber,
			Date BirthDate,BigDecimal MonthSalary,Date EmploymentDate )
	{
		super();
		this.id = id;
		this.JobName = JobName;
		this.FirstName = FirstName;
		this.LastName = LastName;
		this.Pesel = Pesel;
		this.Mail = Mail;
		this.PhoneNumber = PhoneNumber;
		this.BirthDate = BirthDate;
		this.MonthSalary = MonthSalary;
		this.EmploymentDate = EmploymentDate;

	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getJobName() {
		return JobName;
	}

	public void setJobName(String JobName) {
		this.JobName = JobName;
	}

	public String getFirstName() {
		return FirstName;
	}

	public void setFirstName(String FirstName) {
		this.FirstName = FirstName;
	}
	public String getLastName() {
		return FirstName;
	}

	public void setLastName(String FirstName) {
		this.FirstName = FirstName;
	}
	
	public long getPesel() {
		return Pesel;
	}

	public void setPesel(long Pesel) {
		this.Pesel = Pesel;
	}
	

	
	public String getMail() {
		return Mail;
	}

	public void setmail(String Mail) {
		this.Mail = Mail;
	}
	
	public int getPhoneNumber() {
		return PhoneNumber;
	}

	public void setPhoneNumber(int PhoneNumber) {
		this.PhoneNumber = PhoneNumber;
	}
	public Date getBirthDate() {
		return BirthDate;
	}

	public void setBirthDate(Date BirthDate) {
		this.BirthDate = BirthDate;
	}

	public BigDecimal getMonthSalary() {
		return MonthSalary;
	}

	public void setMonthSalary(BigDecimal MonthSalary) {
		this.MonthSalary = MonthSalary;
	}
	public Date getEmploymentDate() {
		return EmploymentDate;
	}

	public void setEmploymentDate(Date EmploymentDate) {
		this.EmploymentDate = EmploymentDate;
	}
//	private int id;
//	private String JobName;
//	private String FirstName;
//	private String LastName;
//	private BigDecimal Pesel;
//	private String Mail;
//	private int PhoneNumber;
//	private String BirthDate;
//	private BigDecimal MonthSalary;
//	private String EmploymentDate;

	

	@Override
	public String toString() {
		return String
				.format("Employee [id=%s, JobName=%s, FirstName=%s, LastName=%s, Pesel=%s , Mail=%s, PhoneNumber=%s, BirthDate=%s,  MonthSalary=%s, EmploymentDate=%s]",
						id, JobName, FirstName,
						LastName, Pesel, Mail, PhoneNumber, BirthDate, MonthSalary, EmploymentDate);
	}
	
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

		
}




