 package core;

import java.math.BigDecimal;


public class View_locations_cities_countries {

	private int id;
	private String StreetAddressLine1;
	private String StreetAddressLine2;
	private String PostalCode;
	private String CityName;
	private String CountryName;
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
	
	public View_locations_cities_countries(int id, String StreetAddressLine1, String StreetAddressLine2, String PostalCode, String CityName, String CountryName)
	{
		super();
		this.id = id;
		this.StreetAddressLine1 = StreetAddressLine1;
		this.StreetAddressLine2 = StreetAddressLine2;
		this.PostalCode = PostalCode;
		this.CityName = CityName;
		this.CountryName = CountryName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getStreetAddressLine1() {
		return StreetAddressLine1;
	}

	public void setStreetAddressLine1(String StreetAddressLine1) {
		this.StreetAddressLine1 = StreetAddressLine1;
	}
	public String getStreetAddressLine2() {
		return StreetAddressLine2;
	}

	public void setStreetAddressLine2(String StreetAddressLine2) {
		this.StreetAddressLine2 = StreetAddressLine2;
	}
	public String getPostalCode() {
		return PostalCode;
	}

	public void setPostalCode(String PostalCode) {
		this.PostalCode = PostalCode;
	}
	public String getCityName() {
		return CityName;
	}

	public void setCityName(String CityName) {
		this.CityName = CityName;
	}
	public String getCountryName() {
		return CountryName;
	}

	public void setCountryName(String CountryName) {
		this.CountryName = CountryName;
	}
	

	

	@Override
	public String toString() {
		return String
				.format("Employee [id=%s, StreetAddressLine1=%s, StreetAddressLine2=%s, PostalCode=%s, CityName=%s, CountryName=%s]",
						id, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName );
	}
	
	//"Id", "ForeName", "Company_LastName",
	//"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };

		
}

