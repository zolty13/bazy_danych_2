package dao;

import java.util.*;
import java.sql.*;
import java.sql.Date;
import java.io.*;
import java.math.BigDecimal;

import core.View_employees_jobs;


public class View_employees_jobsDAO {

	private Connection myConn;
	
	public View_employees_jobsDAO() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("demo.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("DB connection successful to: " + dburl);
	}
	
	public List<View_employees_jobs> getAllView_employees_jobs() throws Exception {
		List<View_employees_jobs> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from View_employees_jobs");
			
			while (myRs.next()) {
				View_employees_jobs tempView_employees_jobs = convertRowToView_employees_jobs(myRs);
				list.add(tempView_employees_jobs);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<View_employees_jobs> searchView_employees_jobs(String LastName) throws Exception {
		List<View_employees_jobs> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			LastName += "%";
			myStmt = myConn.prepareStatement("select * from View_employees_jobs where LastName like ?");
			
			myStmt.setString(1, LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				View_employees_jobs tempView_employees_jobs = convertRowToView_employees_jobs(myRs);
				list.add(tempView_employees_jobs);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private View_employees_jobs convertRowToView_employees_jobs(ResultSet myRs) throws SQLException {
		
		int id = myRs.getInt("id");
		String JobName = myRs.getString("JobName");
		String FirstName = myRs.getString("FirstName");
		String LastName = myRs.getString("LastName");
		long Pesel = myRs.getLong("Pesel");
		String Mail = myRs.getString("Mail");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		Date BirthDate = myRs.getDate("BirthDate");
		BigDecimal MonthSalary = myRs.getBigDecimal("MonthSalary");
		Date EmploymentDate = myRs.getDate("EmploymentDate");

		
		View_employees_jobs tempView_employees_jobs = new View_employees_jobs(id, JobName, FirstName,
				LastName, Pesel, Mail, PhoneNumber, BirthDate, MonthSalary, EmploymentDate);
		
		return tempView_employees_jobs;
	}

	
	private static void close(Connection myConn, Statement myStmt, ResultSet myRs)
			throws SQLException {

		if (myRs != null) {
			myRs.close();
		}

		if (myStmt != null) {
			
		}
		
		if (myConn != null) {
			myConn.close();
		}
	}

	private void close(Statement myStmt, ResultSet myRs) throws SQLException {
		close(null, myStmt, myRs);		
	}

	public static void main(String[] args) throws Exception {
		
		View_employees_jobsDAO dao = new View_employees_jobsDAO();
		System.out.println(dao.searchView_employees_jobs("thom"));

		System.out.println(dao.getAllView_employees_jobs());
	}
}
