package dao;

import java.util.*;
import java.sql.*;
import java.sql.Date;
import java.io.*;
import java.math.BigDecimal;
import java.math.BigInteger;

import core.Client;
import core.Client1;
import core.Employee;
import core.Order;
import core.View_allclients_locations;
import core.View_employees_jobs;
import core.User;
import core.View_entitylegalclients_viewlocationscitiescountries;
import core.View_naturalpersonclients_viewlocationscitiescountries;
import core.View_orders_employees_clients;
import core.view_orders_orderstates_viewlocations;
import core.View_locations_cities_countries;


public class View_allclients_locationsDAO {

	private Connection myConn;
	
	public View_allclients_locationsDAO() throws Exception {
		
		// get db properties
		Properties props = new Properties();
		props.load(new FileInputStream("demo.properties"));
		
		String user = props.getProperty("user");
		String password = props.getProperty("password");
		String dburl = props.getProperty("dburl");
		
		// connect to database
		myConn = DriverManager.getConnection(dburl, user, password);
		
		System.out.println("DB connection successful to: " + dburl);
	}
	
	public void deleteEmployee(int employeeId) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("delete from employees where id=?");
			
			// set param
			myStmt.setInt(1, employeeId);
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
	}
	
	public void deleteOrder(int orderId) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("delete from orders where id=?");
			
			// set param
			myStmt.setInt(1, orderId);
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
	}
	public List<Employee> getAllEmployees() throws Exception {
		List<Employee> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from employees order by id");
			
			while (myRs.next()) {
				Employee tempEmployee = convertRowToEmployee(myRs);
				list.add(tempEmployee);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<Employee> searchEmployees(String lastName) throws Exception {
		List<Employee> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			lastName += "%";
			myStmt = myConn.prepareStatement("select * from employees where lastname like ?  order by Id");
			
			myStmt.setString(1, lastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Employee tempEmployee = convertRowToEmployee(myRs);
				list.add(tempEmployee);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private Employee convertRowToEmployee(ResultSet myRs) throws SQLException {
		int Id = myRs.getInt("Id");
		int JobsId = myRs.getInt("JobsId");
		String firstName = myRs.getString("firstName");
		String lastName = myRs.getString("lastName");
		long Pesel = myRs.getLong("Pesel");
		Date BirthDate = myRs.getDate("BirthDate");
		BigDecimal MonthSalary = myRs.getBigDecimal("MonthSalary");
		Date EmploymentDate = myRs.getDate("EmploymentDate");
		String Mail = myRs.getString("Mail");
		int PhoneNumber = myRs.getInt("PhoneNumber");

		Employee tempEmployee = new Employee(Id,JobsId, firstName, lastName, Pesel, BirthDate,MonthSalary ,EmploymentDate ,Mail , PhoneNumber);
		
		return tempEmployee;
	}
	public List<Client> getAllClients() throws Exception {
		List<Client> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from entitylegalclients order by id");
			
			while (myRs.next()) {
				Client tempClient = convertRowToClient(myRs);
				list.add(tempClient);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<Client> searchClients(String lastName) throws Exception {
		List<Client> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			lastName += "%";
			myStmt = myConn.prepareStatement("select * from entitylegalclients where Forename like ?  order by id");
			
			myStmt.setString(1, lastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Client tempClient = convertRowToClient(myRs);
				list.add(tempClient);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private Client convertRowToClient(ResultSet myRs) throws SQLException {
		int Id = myRs.getInt("Id");
		String Name = myRs.getString("Name");
		long Nip = myRs.getLong("Nip");		
		long Regon = myRs.getLong("Regon");
		int LocationId = myRs.getInt("LocationId");
		String Mail = myRs.getString("Mail");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		

		Client tempClient = new Client(Id,Name, Nip, Regon, LocationId, Mail, PhoneNumber);
		
		return tempClient;
	}

	public void deleteClient(int clientId) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("delete from naturalpersonclients where id=?");
			
			// set param
			myStmt.setInt(1, clientId);
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
	}
	public void addClient(Client theClient) throws Exception {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("insert into entitylegalclients"
					+ " (Name, Nip, Regon, LocationId, Mail, PhoneNumber)"
					+ " values (?, ?, ?, ?,?, ?)");
			
			// set params
			myStmt.setString(1, theClient.getName());
			myStmt.setLong(2, theClient.getNip());
			myStmt.setLong(3, theClient.getRegon());
			myStmt.setInt(4, theClient.getLocationId());
			myStmt.setString(5, theClient.getMail());
			myStmt.setInt(6, theClient.getPhoneNumber());
			

			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	public List<Client1> getAllClient1s() throws Exception {
		List<Client1> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from naturalpersonclients order by id");
			
			while (myRs.next()) {
				Client1 tempClient1 = convertRowToClient1(myRs);
				list.add(tempClient1);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<Client1> searchClient1s(String lastName) throws Exception {
		List<Client1> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			lastName += "%";
			myStmt = myConn.prepareStatement("select * from naturalpersonclients where name like ?  order by id");
			
			myStmt.setString(1, lastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Client1 tempClient1 = convertRowToClient1(myRs);
				list.add(tempClient1);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	public void deleteClient1(int clientId1) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("delete from entitylegalclients where id=?");
			
			// set param
			myStmt.setInt(1, clientId1);
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
	}
	private Client1 convertRowToClient1(ResultSet myRs) throws SQLException {
		int Id = myRs.getInt("Id");
		String ForeName = myRs.getString("ForeName");
		String LastName = myRs.getString("LastName");
		long Pesel = myRs.getLong("Pesel");		
		long Nip = myRs.getLong("Nip");
		int LocationId = myRs.getInt("LocationId");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		String Mail = myRs.getString("Mail");
		

		Client1 tempClient1 = new Client1(Id,ForeName,LastName, Pesel, Nip, LocationId, PhoneNumber, Mail);
		
		return tempClient1;
	}
	
	public void addClient1(Client1 theClient1) throws Exception {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("insert into naturalpersonclients"
					+ " (ForeName, LastName,  Nip,Pesel,LocationId, PhoneNumber, Mail)"
					+ " values (?, ?, ?, ?,?, ?,?)");
			
			// set params
			myStmt.setString(1, theClient1.getForeName());
			myStmt.setString(2, theClient1.getLastName());
			myStmt.setLong(3, theClient1.getPesel());
			myStmt.setLong(4, theClient1.getNip());
			myStmt.setInt(5, theClient1.getLocationId());
			myStmt.setInt(6, theClient1.getPhoneNumber());
			myStmt.setString(7, theClient1.getMail());
			

			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	public void updateEmployee(Employee theEmployee) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("update employees"
					+ " set JobsId=?, firstName=?, lastName=?, Pesel=?, BirthDate=?,MonthSalary=?,EmploymentDate=? , PhoneNumber=?, Mail=? "
					+ " where Id=?");

			// set params
			myStmt.setInt(1, theEmployee.getJobsId());
			myStmt.setString(2, theEmployee.getFirstName());
			myStmt.setString(3, theEmployee.getLastName());
			myStmt.setLong(4, theEmployee.getPesel());
			myStmt.setDate(5, theEmployee.getBirthDate());
			myStmt.setBigDecimal(6, theEmployee.getMonthSalary());
			myStmt.setDate(7, theEmployee.getEmploymentDate());
			myStmt.setInt(8, theEmployee.getPhoneNumber());
			myStmt.setString(9, theEmployee.getMail());
			myStmt.setInt(10, theEmployee.getId());

			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	public void addEmployee(Employee theEmployee) throws Exception {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("insert into employees"
					+ " (Id,JobsId, firstName, lastName, Pesel, BirthDate,MonthSalary ,EmploymentDate , PhoneNumber, Mail )"
					+ " values (?,?, ?, ?, ?,?, ?, ?, ?,?)");
			
			// set params
			myStmt.setInt(1, theEmployee.getId());
			myStmt.setInt(2, theEmployee.getJobsId());
			myStmt.setString(3, theEmployee.getFirstName());
			myStmt.setString(4, theEmployee.getLastName());
			myStmt.setLong(5, theEmployee.getPesel());
			myStmt.setDate(6, theEmployee.getBirthDate());
			myStmt.setBigDecimal(7, theEmployee.getMonthSalary());
			myStmt.setDate(8, theEmployee.getEmploymentDate());
			myStmt.setInt(9, theEmployee.getPhoneNumber());
			myStmt.setString(10, theEmployee.getMail());

			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	
	
	
	public List<Order> getAllOrders() throws Exception {
		List<Order> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from Orders Order by id");
			
			while (myRs.next()) {
				Order tempOrder = convertRowToOrder(myRs);
				list.add(tempOrder);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<Order> searchOrders(String lastName) throws Exception {
		List<Order> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			lastName += "%";
			myStmt = myConn.prepareStatement("select * from Orders where ClientId like ?  Order by id");
			
			myStmt.setString(1, lastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				Order tempOrder = convertRowToOrder(myRs);
				list.add(tempOrder);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private Order convertRowToOrder(ResultSet myRs) throws SQLException {
		
		int Id = myRs.getInt("Id");
		boolean IfLegalEntity = myRs.getBoolean("IfLegalEntity");
		int ClientId = myRs.getInt("ClientId");
		int OrderStateId = myRs.getInt("OrderStateId");
		int DestinationLocationId = myRs.getInt("DestinationLocationId");
		int SourceLocationId = myRs.getInt("SourceLocationId");
		Date Deadline = myRs.getDate("Deadline");
		BigDecimal Price = myRs.getBigDecimal("Price");
		String Description = myRs.getString("Description");
		Date DepartureDate = myRs.getDate("DepartureDate");


		Order tempOrder = new Order(Id,IfLegalEntity, ClientId, OrderStateId, DestinationLocationId,
				 SourceLocationId, Description, Deadline, Price, DepartureDate);
		
		return tempOrder;
	}
	public void updateOrder(Order theOrder) throws SQLException {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("update orders"
			     	+  " set IfLegalEntity=?, ClientId=?, OrderStateId=?, DestinationLocationId=?,"
					+" SourceLocationId=?, Description=?, Deadline=?, Price=?, DepartureDate=?"
					+ " where id=?");
			
			// set params
			myStmt.setBoolean(1, theOrder.getIfLegalEntity());
			myStmt.setInt(2, theOrder.getClientId());
			myStmt.setInt(3, theOrder.getOrderStateId());
			myStmt.setInt(4, theOrder.getDestinationLocationId());
			myStmt.setInt(5, theOrder.getSourceLocationId());
			myStmt.setString(6, theOrder.getDescription());
			myStmt.setDate(7, theOrder.getDeadline());
			myStmt.setBigDecimal(8, theOrder.getPrice());
			myStmt.setDate(9, theOrder.getDepartureDate());
			myStmt.setInt(10, theOrder.getId());
			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	public void addOrder(Order theOrder) throws Exception {
		PreparedStatement myStmt = null;

		try {
			// prepare statement
			myStmt = myConn.prepareStatement("insert into Orders"
					+ " (IfLegalEntity, ClientId,DestinationLocationId, OrderStateId, SourceLocationId, Description,Deadline ,Price ,DepartureDate )"
					+ " values (?, ?, ?, ?,?, ?, ?,?,?)");
			
			// set params
			myStmt.setBoolean(1, theOrder.getIfLegalEntity());
			myStmt.setInt(2, theOrder.getClientId());
			myStmt.setInt(3, theOrder.getDestinationLocationId());
			myStmt.setInt(4, theOrder.getOrderStateId());
			myStmt.setInt(5, theOrder.getSourceLocationId());
			myStmt.setString(6, theOrder.getDescription());
			myStmt.setDate(7, theOrder.getDeadline());
			myStmt.setBigDecimal(8, theOrder.getPrice());
			myStmt.setDate(9, theOrder.getDepartureDate());


			
			// execute SQL
			myStmt.executeUpdate();			
		}
		finally {
			close(myStmt);
		}
		
	}
	public List<View_allclients_locations> getAllView_allclients_locations() throws Exception {
		List<View_allclients_locations> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from View_allclients_locations");
			
			while (myRs.next()) {
				View_allclients_locations tempView_allclients_locations = convertRowToView_allclients_locations(myRs);
				list.add(tempView_allclients_locations);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<View_employees_jobs> getAllView_employees_jobs() throws Exception {
		List<View_employees_jobs> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from View_employees_jobs");
			
			while (myRs.next()) {
				View_employees_jobs tempView_employees_jobs = convertRowToView_employees_jobs(myRs);
				list.add(tempView_employees_jobs);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	public List<View_employees_jobs> searchView_employees_jobs(String LastName) throws Exception {
		List<View_employees_jobs> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			LastName += "%";
			myStmt = myConn.prepareStatement("select * from View_employees_jobs where LastName like ?");
			
			myStmt.setString(1, LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				View_employees_jobs tempView_employees_jobs = convertRowToView_employees_jobs(myRs);
				list.add(tempView_employees_jobs);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
private View_employees_jobs convertRowToView_employees_jobs(ResultSet myRs) throws SQLException {
		
		int id = myRs.getInt("id");
		String JobName = myRs.getString("JobName");
		String FirstName = myRs.getString("FirstName");
		String LastName = myRs.getString("LastName");
		long Pesel = myRs.getLong("Pesel");
		String Mail = myRs.getString("Mail");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		Date BirthDate = myRs.getDate("BirthDate");
		BigDecimal MonthSalary = myRs.getBigDecimal("MonthSalary");
		Date EmploymentDate = myRs.getDate("EmploymentDate");

		
		View_employees_jobs tempView_employees_jobs = new View_employees_jobs(id, JobName, FirstName,
				LastName, Pesel, Mail, PhoneNumber, BirthDate, MonthSalary, EmploymentDate);
		
		return tempView_employees_jobs;
	}
public List<View_entitylegalclients_viewlocationscitiescountries> getAllView_entitylegalclients_viewlocationscitiescountries() throws Exception {
	List<View_entitylegalclients_viewlocationscitiescountries> list = new ArrayList<>();
	
	Statement myStmt = null;
	ResultSet myRs = null;
	
	try {
		myStmt = myConn.createStatement();
		myRs = myStmt.executeQuery("select * from View_entitylegalclients_viewlocationscitiescountries");
		
		while (myRs.next()) {
			View_entitylegalclients_viewlocationscitiescountries tempView_entitylegalclients_viewlocationscitiescountries = convertRowToView_entitylegalclients_viewlocationscitiescountries(myRs);
			list.add(tempView_entitylegalclients_viewlocationscitiescountries);
		}

		return list;		
	}
	finally {
		close(myStmt, myRs);
	}
}

public List<View_entitylegalclients_viewlocationscitiescountries> searchView_entitylegalclients_viewlocationscitiescountries(String LastName) throws Exception {
	List<View_entitylegalclients_viewlocationscitiescountries> list = new ArrayList<>();

	PreparedStatement myStmt = null;
	ResultSet myRs = null;

	try {
		LastName += "%";
		myStmt = myConn.prepareStatement("select * from View_entitylegalclients_viewlocationscitiescountries where Name like ?");
		
		myStmt.setString(1, LastName);
		
		myRs = myStmt.executeQuery();
		
		while (myRs.next()) {
			View_entitylegalclients_viewlocationscitiescountries tempView_entitylegalclients_viewlocationscitiescountries = convertRowToView_entitylegalclients_viewlocationscitiescountries(myRs);
			list.add(tempView_entitylegalclients_viewlocationscitiescountries);
		}
		
		return list;
	}
	finally {
		close(myStmt, myRs);
	}
}
private View_entitylegalclients_viewlocationscitiescountries convertRowToView_entitylegalclients_viewlocationscitiescountries(ResultSet myRs) throws SQLException {
	
	int id = myRs.getInt("id");
	String Name = myRs.getString("Name");
	long Regon = myRs.getLong("Regon");
	long NIP = myRs.getLong("NIP");
	String Mail = myRs.getString("Mail");
	int PhoneNumber = myRs.getInt("PhoneNumber");
	String StreetAddressLine1 = myRs.getString("StreetAddressLine1");
	String StreetAddressLine2 = myRs.getString("StreetAddressLine2");
	int PostalCode = myRs.getInt("PostalCode");
	String CityName = myRs.getString("CityName");
	String CountryName = myRs.getString("CountryName");


	
	View_entitylegalclients_viewlocationscitiescountries tempView_entitylegalclients_viewlocationscitiescountries = new View_entitylegalclients_viewlocationscitiescountries( id,  Name,  NIP, Regon,  Mail,
			  PhoneNumber,  StreetAddressLine1,  StreetAddressLine2,  PostalCode,  CityName,  CountryName);
	
	return tempView_entitylegalclients_viewlocationscitiescountries;
}
public List<View_locations_cities_countries> getAllview_locations_cities_countries() throws Exception {
	List<View_locations_cities_countries> list = new ArrayList<>();
	
	Statement myStmt = null;
	ResultSet myRs = null;
	
	try {
		myStmt = myConn.createStatement();
		myRs = myStmt.executeQuery("select * from view_locations_cities_countries");
		
		while (myRs.next()) {
			View_locations_cities_countries tempview_locations_cities_countries = convertRowToview_locations_cities_countries(myRs);
			list.add(tempview_locations_cities_countries);
		}

		return list;		
	}
	finally {
		close(myStmt, myRs);
	}
}

public List<View_locations_cities_countries> searchview_locations_cities_countries(String LastName) throws Exception {
	List<View_locations_cities_countries> list = new ArrayList<>();

	PreparedStatement myStmt = null;
	ResultSet myRs = null;

	try {
		LastName += "%";
		myStmt = myConn.prepareStatement("select * from view_locations_cities_countries where CountryName like ?");
		
		myStmt.setString(1, LastName);
		
		myRs = myStmt.executeQuery();
		
		while (myRs.next()) {
			View_locations_cities_countries tempview_locations_cities_countries = convertRowToview_locations_cities_countries(myRs);
			list.add(tempview_locations_cities_countries);
		}
		
		return list;
	}
	finally {
		close(myStmt, myRs);
	}
}
private View_locations_cities_countries convertRowToview_locations_cities_countries(ResultSet myRs) throws SQLException {
	
	int id = myRs.getInt("id");

	String StreetAddressLine1 = myRs.getString("StreetAddressLine1");
	String StreetAddressLine2 = myRs.getString("StreetAddressLine2");
	String PostalCode = myRs.getString("PostalCode");
	String CityName = myRs.getString("CityName");
	String CountryName = myRs.getString("CountryName");


	
	View_locations_cities_countries tempview_locations_cities_countries = new View_locations_cities_countries( id,  StreetAddressLine1,  StreetAddressLine2,  PostalCode,  CityName,  CountryName);
	
	return tempview_locations_cities_countries;
}




	public List<View_allclients_locations> searchView_allclients_locations(String Company_LastName) throws Exception {
		List<View_allclients_locations> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			Company_LastName += "%";
			myStmt = myConn.prepareStatement("select * from View_allclients_locations where Company_LastName like ?");
			
			myStmt.setString(1, Company_LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				View_allclients_locations tempView_allclients_locations = convertRowToView_allclients_locations(myRs);
				list.add(tempView_allclients_locations);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private View_allclients_locations convertRowToView_allclients_locations(ResultSet myRs) throws SQLException {
		
		int id = myRs.getInt("id");
		String Company_LastName = myRs.getString("Company_LastName");
		String ForeName = myRs.getString("ForeName");
		String Mail = myRs.getString("mail");
		long Pesel_Regon = myRs.getLong("Pesel_Regon");
		long NIP = myRs.getLong("NIP");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		String StreetAddressLine1 = myRs.getString("StreetAddressLine1");
		String StreetAddressLine2 = myRs.getString("StreetAddressLine2");
		String PostalCode = myRs.getString("PostalCode");
		String CityName = myRs.getString("CityName");
		String CountryName = myRs.getString("CountryName");
		Boolean IfLegalEntity = myRs.getBoolean("IfLegalEntity");

		
		View_allclients_locations tempView_allclients_locations = new View_allclients_locations(id, ForeName, Company_LastName,
				Pesel_Regon, NIP, Mail, PhoneNumber, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName, IfLegalEntity);
		
		return tempView_allclients_locations;
	}

	public List<View_naturalpersonclients_viewlocationscitiescountries> getAllView_naturalpersonclients_viewlocationscitiescountries() throws Exception {
		List<View_naturalpersonclients_viewlocationscitiescountries> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from View_naturalpersonclients_viewlocationscitiescountries");
			
			while (myRs.next()) {
				View_naturalpersonclients_viewlocationscitiescountries tempView_naturalpersonclients_viewlocationscitiescountries = convertRowToView_naturalpersonclients_viewlocationscitiescountries(myRs);
				list.add(tempView_naturalpersonclients_viewlocationscitiescountries);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	
	
		public List<View_naturalpersonclients_viewlocationscitiescountries> searchView_naturalpersonclients_viewlocationscitiescountries(String LastName) throws Exception {
		List<View_naturalpersonclients_viewlocationscitiescountries> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			LastName += "%";
			myStmt = myConn.prepareStatement("select * from View_naturalpersonclients_viewlocationscitiescountries where ForeName like ?");
			
			myStmt.setString(1, LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				View_naturalpersonclients_viewlocationscitiescountries tempView_naturalpersonclients_viewlocationscitiescountries = convertRowToView_naturalpersonclients_viewlocationscitiescountries(myRs);
				list.add(tempView_naturalpersonclients_viewlocationscitiescountries);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private View_naturalpersonclients_viewlocationscitiescountries convertRowToView_naturalpersonclients_viewlocationscitiescountries(ResultSet myRs) throws SQLException {
		
		int id = myRs.getInt("id");
		String ForeName = myRs.getString("ForeName");
		String LastName = myRs.getString("LastName");
		String Mail = myRs.getString("mail");
		long Pesel = myRs.getLong("Pesel");
		int NIP = myRs.getInt("NIP");
		int PhoneNumber = myRs.getInt("PhoneNumber");
		String StreetAddressLine1 = myRs.getString("StreetAddressLine1");
		String StreetAddressLine2 = myRs.getString("StreetAddressLine2");
		String PostalCode = myRs.getString("PostalCode");
		String CityName = myRs.getString("CityName");
		String CountryName = myRs.getString("CountryName");

		
		View_naturalpersonclients_viewlocationscitiescountries tempView_naturalpersonclients_viewlocationscitiescountries = new View_naturalpersonclients_viewlocationscitiescountries(id, ForeName, LastName,
				Pesel, NIP, Mail, PhoneNumber, StreetAddressLine1, StreetAddressLine2, PostalCode, CityName, CountryName);
		
		return tempView_naturalpersonclients_viewlocationscitiescountries;
	}
	
	

	public List<View_orders_employees_clients> getAllView_orders_employees_clients() throws Exception {
		List<View_orders_employees_clients> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from View_orders_employees_clients");
			
			while (myRs.next()) {
				View_orders_employees_clients tempView_orders_employees_clients = convertRowToView_orders_employees_clients(myRs);
				list.add(tempView_orders_employees_clients);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	
	
		public List<View_orders_employees_clients> searchView_orders_employees_clients(String LastName) throws Exception {
		List<View_orders_employees_clients> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			LastName += "%";
			myStmt = myConn.prepareStatement("select * from View_orders_employees_clients where ForeName like ?");
			
			myStmt.setString(1, LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				View_orders_employees_clients tempView_orders_employees_clients = convertRowToView_orders_employees_clients(myRs);
				list.add(tempView_orders_employees_clients);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private View_orders_employees_clients convertRowToView_orders_employees_clients(ResultSet myRs) throws SQLException {
		
		int Orderid = myRs.getInt("Orderid");
		String State = myRs.getString("State");
		boolean IfLegalEntity = myRs.getBoolean("IfLegalEntity");
		String ClientId = myRs.getString("ClientId");
		String ForeName = myRs.getString("ForeName");
		String Company_LastName = myRs.getString("Company_LastName");
		long Pesel_Regon = myRs.getLong("Pesel_Regon");
		String Cmail = myRs.getString("Cmail");
		int CPhone = myRs.getInt("CPhone");
		int DestinationLocationID = myRs.getInt("DestinationLocationID");
		int SourceLocationID = myRs.getInt("SourceLocationID");
		String Description = myRs.getString("Description");
		Date Deadline = myRs.getDate("Deadline");
		BigDecimal Price = myRs.getBigDecimal("Price");
		Date DepartureDate = myRs.getDate("DepartureDate");
		int EmpId = myRs.getInt("EmpId");
		String FirstName = myRs.getString("FirstName");
		String LastName = myRs.getString("LastName");
		long Pesel = myRs.getLong("Pesel");
		String Mail = myRs.getString("Mail");
		int PhoneNumber = myRs.getInt("PhoneNumber");

		View_orders_employees_clients tempView_orders_employees_clients = new View_orders_employees_clients(Orderid, State, 
				IfLegalEntity, ClientId, ForeName
				, Company_LastName,Pesel_Regon, Cmail, CPhone, DestinationLocationID,
				SourceLocationID, Description, Deadline, Price, 
				DepartureDate, EmpId, FirstName, 
				LastName, Pesel, Mail,PhoneNumber);
		
		return tempView_orders_employees_clients;
	}
	public List<view_orders_orderstates_viewlocations> getAllview_orders_orderstates_viewlocations() throws Exception {
		List<view_orders_orderstates_viewlocations> list = new ArrayList<>();
		
		Statement myStmt = null;
		ResultSet myRs = null;
		
		try {
			myStmt = myConn.createStatement();
			myRs = myStmt.executeQuery("select * from view_orders_orderstates_viewlocations");
			
			while (myRs.next()) {
				view_orders_orderstates_viewlocations tempview_orders_orderstates_viewlocations = convertRowToview_orders_orderstates_viewlocations(myRs);
				list.add(tempview_orders_orderstates_viewlocations);
			}

			return list;		
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	
	
	
		public List<view_orders_orderstates_viewlocations> searchview_orders_orderstates_viewlocations(String LastName) throws Exception {
		List<view_orders_orderstates_viewlocations> list = new ArrayList<>();

		PreparedStatement myStmt = null;
		ResultSet myRs = null;

		try {
			LastName += "%";
			myStmt = myConn.prepareStatement("select * from view_orders_orderstates_viewlocations where Deadline like ?");
			
			myStmt.setString(1, LastName);
			
			myRs = myStmt.executeQuery();
			
			while (myRs.next()) {
				view_orders_orderstates_viewlocations tempview_orders_orderstates_viewlocations = convertRowToview_orders_orderstates_viewlocations(myRs);
				list.add(tempview_orders_orderstates_viewlocations);
			}
			
			return list;
		}
		finally {
			close(myStmt, myRs);
		}
	}
	
	private view_orders_orderstates_viewlocations convertRowToview_orders_orderstates_viewlocations(ResultSet myRs) throws SQLException {
//		private int Id;
//		private String State;
//		private	boolean IfLegalEntity;
//		private String ClientId;
//		private Date Deadline;
//		private Date DepartureDate;
//		private String Description;
//		private BigDecimal Price;
//		private String Street_Dest;
//		private String Address_Dest;
//		private String Code_Dest;
//		private String City_Dest;
//		private String Country_Dest;
//		private String Street_Src;
//		private String Address_Src;
//		private String Code_Src;
//		private String City_Src;
//		private String Country_Src;
		int Id = myRs.getInt("Id");
		String State = myRs.getString("State");
		boolean IfLegalEntity = myRs.getBoolean("IfLegalEntity");
		String ClientId = myRs.getString("ClientId");
		Date Deadline = myRs.getDate("Deadline");
		Date DepartureDate = myRs.getDate("DepartureDate");
		String Description = myRs.getString("Description");
		BigDecimal Price = myRs.getBigDecimal("Price");
		String Street_Dest = myRs.getString("Street Dest");
		String Address_Dest = myRs.getString("Address Dest");
		String Code_Dest = myRs.getString("Code Dest");
		String City_Dest = myRs.getString("City Dest");
		String Country_Dest = myRs.getString("Country Dest");
		String Street_Src = myRs.getString("Street Src");
		String Address_Src = myRs.getString("Address Src");
		String Code_Src = myRs.getString("Code Src");
		String City_Src = myRs.getString("City Src");
		String Country_Src = myRs.getString("Country Src");

	

		view_orders_orderstates_viewlocations tempview_orders_orderstates_viewlocations = new view_orders_orderstates_viewlocations(
				Id, State, IfLegalEntity, ClientId, Deadline, DepartureDate, Description, Price, Street_Dest,
				Address_Dest, Code_Dest, City_Dest, Country_Dest, Street_Src, Address_Src, Code_Src, City_Src,
				Country_Src);
		
		return tempview_orders_orderstates_viewlocations;
	}
	private static void close(Connection myConn, Statement myStmt, ResultSet myRs)
			throws SQLException {

		if (myRs != null) {
			myRs.close();
		}

		if (myStmt != null) {
			
		}
		
		if (myConn != null) {
			myConn.close();
		}
	}



	private static void close(Statement myStmt, ResultSet myRs) throws SQLException {
		close(null, myStmt, myRs);		
	}
	private void close(Statement myStmt) throws SQLException {
		close(null, myStmt, null);		
	}

	public static void main(String[] args) throws Exception {
		
		View_allclients_locationsDAO dao = new View_allclients_locationsDAO();
		System.out.println(dao.searchView_allclients_locations("thom"));

		System.out.println(dao.getAllView_allclients_locations());
		
		View_employees_jobsDAO dao1 = new View_employees_jobsDAO();
		System.out.println(dao1.searchView_employees_jobs("thom"));

		System.out.println(dao1.getAllView_employees_jobs());
		
		View_allclients_locationsDAO dao3 = new View_allclients_locationsDAO();
		System.out.println(dao3.searchView_entitylegalclients_viewlocationscitiescountries("thom"));

		System.out.println(dao3.getAllView_entitylegalclients_viewlocationscitiescountries());
		
		View_allclients_locationsDAO dao4 = new View_allclients_locationsDAO();
		System.out.println(dao4.searchview_locations_cities_countries("thom"));

		System.out.println(dao4.getAllview_locations_cities_countries());
		
		
		
	}
}
