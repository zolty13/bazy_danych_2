
	package ui;

	import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.View_locations_cities_countries;
	class View_locations_cities_countriesTableModel extends AbstractTableModel {

		private static final int Id_COL = 0;

		private static final int StreetAddressLine1_COL = 1;
		private static final int StreetAddressLine2_COL = 2;
		private static final int PostalCode_COL = 3;
		private static final int CityName_COL = 4;
		private static final int CountryName_COL = 5;


		private String[] columnNames = { "id", 
				 "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName"  };
		private List<View_locations_cities_countries> view_locations_cities_countries;

		public View_locations_cities_countriesTableModel(List<View_locations_cities_countries> theview_locations_cities_countries) {
			view_locations_cities_countries = theview_locations_cities_countries;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return view_locations_cities_countries.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			View_locations_cities_countries tempview_locations_cities_countries = view_locations_cities_countries.get(row);
//			private int id;
//			private String Name;
//			private BigDecimal NIP;
//			private BigDecimal Regon;
//			private String Mail;
//			private int PhoneNumber;
//			private String StreetAddressLine1;
//			private String StreetAddressLine2;
//			private int PostalCode;
//			private String CityName;
//			private String CountryName;
			switch (col) {
			case Id_COL:
				return tempview_locations_cities_countries.getId();
			case StreetAddressLine1_COL:
				return tempview_locations_cities_countries.getStreetAddressLine1();
			case StreetAddressLine2_COL:
				return tempview_locations_cities_countries.getStreetAddressLine2();
			case PostalCode_COL:
				return tempview_locations_cities_countries.getPostalCode();
			case CityName_COL:
				return tempview_locations_cities_countries.getCityName();
			case CountryName_COL:
				return tempview_locations_cities_countries.getCountryName();

				
			default:
				return tempview_locations_cities_countries.getId();
			}
		}



	}
