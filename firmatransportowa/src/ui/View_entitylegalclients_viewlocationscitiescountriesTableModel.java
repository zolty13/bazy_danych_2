package ui;

import java.util.List;

import javax.swing.table.AbstractTableModel;



import core.View_entitylegalclients_viewlocationscitiescountries;
class View_entitylegalclients_viewlocationscitiescountriesTableModel extends AbstractTableModel {

	private static final int Id_COL = 0;
	private static final int Name_COL = 1;
	private static final int NIP_COL = 2;
	private static final int Regon_COL = 3;
	private static final int Mail_COL = 4;
	private static final int PhoneNumber_COL = 5;
	private static final int StreetAddressLine1_COL = 6;
	private static final int StreetAddressLine2_NAME_COL = 7;
	private static final int PostalCode_COL = 8;
	private static final int CityName_COL = 9;
	private static final int CountryName_COL = 10;

	private String[] columnNames = { "id", "Name", 
			"NIP", "Regon",  "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName"  };
	private List<View_entitylegalclients_viewlocationscitiescountries> View_entitylegalclients_viewlocationscitiescountries;

	public View_entitylegalclients_viewlocationscitiescountriesTableModel(List<View_entitylegalclients_viewlocationscitiescountries> theView_entitylegalclients_viewlocationscitiescountries) {
		View_entitylegalclients_viewlocationscitiescountries = theView_entitylegalclients_viewlocationscitiescountries;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return View_entitylegalclients_viewlocationscitiescountries.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		View_entitylegalclients_viewlocationscitiescountries tempView_entitylegalclients_viewlocationscitiescountries = View_entitylegalclients_viewlocationscitiescountries.get(row);
//		private int id;
//		private String Name;
//		private BigDecimal NIP;
//		private BigDecimal Regon;
//		private String Mail;
//		private int PhoneNumber;
//		private String StreetAddressLine1;
//		private String StreetAddressLine2;
//		private int PostalCode;
//		private String CityName;
//		private String CountryName;
		switch (col) {
		case Id_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getId();
		case Name_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getName();
		case NIP_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getNIP();
		case Regon_COL:
				return tempView_entitylegalclients_viewlocationscitiescountries.getRegon();
		case Mail_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getMail();
		case PhoneNumber_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getPhoneNumber();
		case StreetAddressLine1_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getStreetAddressLine1();
		case StreetAddressLine2_NAME_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getStreetAddressLine2();
		case PostalCode_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getPostalCode();
		case CityName_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getCityName();
		case CountryName_COL:
			return tempView_entitylegalclients_viewlocationscitiescountries.getCountryName();

			
		default:
			return tempView_entitylegalclients_viewlocationscitiescountries.getName();
		}
	}



}
