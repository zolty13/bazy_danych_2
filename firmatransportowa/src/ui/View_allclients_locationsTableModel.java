package ui;

import java.util.List;

import javax.swing.table.AbstractTableModel;



import core.View_allclients_locations;

class View_allclients_locationsTableModel extends AbstractTableModel {

	private static final int Id_COL = 0;
	private static final int ForeName_COL = 1;
	private static final int Company_LastName_COL = 2;
	private static final int Pesel_Regon_COL = 3;
	private static final int NIP_COL = 4;
	private static final int Mail_COL = 5;
	private static final int PhoneNumber_COL = 6;
	private static final int StreetAddressLine1_COL = 7;
	private static final int StreetAddressLine2_NAME_COL = 8;
	private static final int PostalCode_COL = 9;
	private static final int CityName_COL = 10;
	private static final int CountryName_COL = 11;
	private static final int IfLegalEntity_COL = 12;


	private String[] columnNames = { "Id", "ForeName", "Company_LastName",
			"Pesel_Regon", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1",
"StreetAddressLine2", "PostalCode", "CityName", "CountryName", "IfLegalEntity" };
	private List<View_allclients_locations> View_allclients_locations;

	public View_allclients_locationsTableModel(List<View_allclients_locations> theView_allclients_locations) {
		View_allclients_locations = theView_allclients_locations;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return View_allclients_locations.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		View_allclients_locations tempView_allclients_locations = View_allclients_locations.get(row);

		switch (col) {
		case Id_COL:
			return tempView_allclients_locations.getId();
		case ForeName_COL:
			return tempView_allclients_locations.getForeName();
		case Company_LastName_COL:
			return tempView_allclients_locations.getCompany_LastName();
		case Pesel_Regon_COL:
			return tempView_allclients_locations.getPesel_Regon();
		case NIP_COL:
			return tempView_allclients_locations.getNIP();
		case Mail_COL:
			return tempView_allclients_locations.getMail();
		case PhoneNumber_COL:
			return tempView_allclients_locations.getPhoneNumber();
		case StreetAddressLine1_COL:
			return tempView_allclients_locations.getStreetAddressLine1();
		case StreetAddressLine2_NAME_COL:
			return tempView_allclients_locations.getStreetAddressLine2();
		case PostalCode_COL:
			return tempView_allclients_locations.getPostalCode();
		case CityName_COL:
			return tempView_allclients_locations.getCityName();
		case CountryName_COL:
			return tempView_allclients_locations.getCountryName();
		case IfLegalEntity_COL:
			return tempView_allclients_locations.getIfLegalEntity();

			
		default:
			return tempView_allclients_locations.getCompany_LastName();
		}
	}



}
