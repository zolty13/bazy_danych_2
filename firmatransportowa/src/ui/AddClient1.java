package ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;


import core.Client1;
import dao.View_allclients_locationsDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class AddClient1 extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField NametextField;
	private JTextField LocationIdtextField;
	private JTextField NiptextField;
	private JTextField RegontextField;
	private JTextField MailtextField;
	private JTextField PhoneNumbertextField;

	private View_allclients_locationsDAO view_allClient1s_locationsDAO;
	private View_allclients_locationsSearchApp view_allClient1s_locationsSearchApp;
	private JTextField Name1textField;
	private JTextField IdtextField;
	/**
	 * Launch the application.
	 */	
	
	
	public AddClient1(View_allclients_locationsSearchApp theview_allClient1s_locationsSearchApp, View_allclients_locationsDAO theview_allClient1s_locationsDAO) {
			this();
			view_allClient1s_locationsDAO = theview_allClient1s_locationsDAO;
			view_allClient1s_locationsSearchApp = theview_allClient1s_locationsSearchApp;
		}
	public static void main(String[] args) {
		try {
			AddClient1 dialog = new AddClient1();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddClient1() {
		setTitle("Dodaj Klienta (natural)");

		setBounds(100, 100, 463, 321);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.NORTH);
		contentPanel.setLayout(new MigLayout("", "[84px][341px,grow]", "[][20px][][20px][20px][20px][20px][20px][20px][20px][20px]"));
		{
			JLabel lblId = new JLabel("Id");
			contentPanel.add(lblId, "cell 0 0,alignx trailing");
		}
		{
			IdtextField = new JTextField();
			IdtextField.setColumns(10);
			contentPanel.add(IdtextField, "cell 1 0,growx");
		}
		{
			JLabel lblFirstName_1 = new JLabel("ForeName");
			contentPanel.add(lblFirstName_1, "cell 0 1,alignx right,aligny center");
		}
		{
			NametextField = new JTextField();
			contentPanel.add(NametextField, "cell 1 1,growx,aligny top");
			NametextField.setColumns(10);
		}
		{
			JLabel lblLastname = new JLabel("LastName");
			contentPanel.add(lblLastname, "cell 0 2,alignx trailing");
		}
		{
			Name1textField = new JTextField();
			Name1textField.setColumns(10);
			contentPanel.add(Name1textField, "cell 1 2,growx");
		}
		{
			JLabel lblPesel = new JLabel("Pesel");
			contentPanel.add(lblPesel, "cell 0 3,alignx right,aligny center");
		}
		{
			NiptextField = new JTextField();
			contentPanel.add(NiptextField, "cell 1 3,growx,aligny top");
			NiptextField.setColumns(10);
		}
		{
			JLabel lblMonthSalary = new JLabel("Nip");
			contentPanel.add(lblMonthSalary, "cell 0 4,alignx right,aligny center");
		}
		{
			RegontextField = new JTextField();
			contentPanel.add(RegontextField, "cell 1 4,growx,aligny top");
			RegontextField.setColumns(10);
		}
		{
			JLabel lblLocationid = new JLabel("LocationId");
			contentPanel.add(lblLocationid, "cell 0 5,alignx trailing");
		}
		{
			LocationIdtextField = new JTextField();
			contentPanel.add(LocationIdtextField, "cell 1 5,growx,aligny top");
			LocationIdtextField.setColumns(10);
		}
		{
			JLabel lblPhoneNumber = new JLabel("Phone Number");
			contentPanel.add(lblPhoneNumber, "cell 0 6,alignx right,aligny center");
		}
		{
			PhoneNumbertextField = new JTextField();
			PhoneNumbertextField.setText("");
			contentPanel.add(PhoneNumbertextField, "cell 1 6,growx,aligny top");
			PhoneNumbertextField.setColumns(10);
		}
		{
			JLabel lblMail = new JLabel("Mail");
			contentPanel.add(lblMail, "cell 0 7,alignx right,aligny center");
		}
		{
			MailtextField = new JTextField();
			contentPanel.add(MailtextField, "cell 1 7,growx,aligny top");
			MailtextField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						saveClient1();
					}

					
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	protected BigDecimal convertStringToBigDecimal(String salaryStr) {

		BigDecimal result = null;

		try {
			double salaryDouble = Double.parseDouble(salaryStr);

			result = BigDecimal.valueOf(salaryDouble);
		} catch (Exception exc) {
			System.out.println("Invalid value. Defaulting to 0.0");
			result = BigDecimal.valueOf(0.0);
		}

		return result;
	}
	
	
	
	protected void saveClient1() {
	
		// get the Client1 info from gui
		//Name, Nip, Regon, LocationId, Mail, PhoneNumber
		String Idstr = IdtextField.getText();
		String ForeName = NametextField.getText();
		String LastName = Name1textField.getText();
		String PeselStr = RegontextField.getText();
		String Nipstr = NiptextField.getText();
		String LocationIdstr = LocationIdtextField.getText();
		String PhoneNumberstr = PhoneNumbertextField.getText();
		String Mail = MailtextField.getText();
		

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date textFieldAsDate = null;
		java.util.Date textFieldAsDate1 = null;

		
		sdf = new SimpleDateFormat("yyyy-MM-dd");


		
		int LocationId = Integer.parseInt(LocationIdstr);
		int PhoneNumber = Integer.parseInt(PhoneNumberstr);
		int Id = Integer.parseInt(Idstr);
		long Pesel = Long.parseLong(PeselStr);
		long Nip = Long.parseLong(Nipstr);
		Client1 tempClient1 = new Client1(Id,ForeName,LastName, Pesel, Nip, LocationId, PhoneNumber, Mail);
		
		try {
			// save to the database
			view_allClient1s_locationsDAO.addClient1(tempClient1);

			// close dialog
			setVisible(false);
			dispose();

			// refresh gui list
			view_allClient1s_locationsSearchApp.refreshClient1sView();
			
			// show success message
			JOptionPane.showMessageDialog(view_allClient1s_locationsSearchApp,
					"Client1 added succesfully.",
					"Client1 Added",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(
					view_allClient1s_locationsSearchApp,
					"Error saving Client1: "
							+ exc.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
}
