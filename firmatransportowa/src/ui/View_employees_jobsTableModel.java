package ui;

import java.util.List;

import javax.swing.table.AbstractTableModel;



import core.View_employees_jobs;


class View_employees_jobsTableModel extends AbstractTableModel {

	private static final int Id_COL = 0;
	private static final int JobName_COL = 1;
	private static final int FirstName_COL = 2;
	private static final int LastName_COL = 3;
	private static final int Pesel_COL = 4;
	private static final int Mail_COL = 5;
	private static final int PhoneNumber_COL = 6;
	private static final int BirthDate_COL = 7;
	private static final int MonthSalary_COL = 8;
	private static final int EmploymentDate_COL = 9;



	private String[] columnNames = { "id", "JobName", "FirstName",
			"LastName", "Pesel","Mail", "PhoneNumber", "BirthDate", "MonthSalary", "EmploymentDate" };
	private List<View_employees_jobs> View_employees_jobs;

	public View_employees_jobsTableModel(List<View_employees_jobs> theView_employees_jobs) {
		View_employees_jobs = theView_employees_jobs;
	}

	@Override
	public int getColumnCount() {
		return columnNames.length;
	}

	@Override
	public int getRowCount() {
		return View_employees_jobs.size();
	}

	@Override
	public String getColumnName(int col) {
		return columnNames[col];
	}

	@Override
	public Object getValueAt(int row, int col) {

		View_employees_jobs tempView_employees_jobs = View_employees_jobs.get(row);

		switch (col) {
		case Id_COL:
			return tempView_employees_jobs.getId();
		case JobName_COL:
			return tempView_employees_jobs.getJobName();
		case FirstName_COL:
			return tempView_employees_jobs.getFirstName();
		case LastName_COL:
			return tempView_employees_jobs.getLastName();
		case Pesel_COL:
			return tempView_employees_jobs.getPesel();
		case Mail_COL:
			return tempView_employees_jobs.getMail();
		case PhoneNumber_COL:
			return tempView_employees_jobs.getPhoneNumber();
		case BirthDate_COL:
			return tempView_employees_jobs.getBirthDate();
		case MonthSalary_COL:
			return tempView_employees_jobs.getMonthSalary();
		case EmploymentDate_COL:
			return tempView_employees_jobs.getEmploymentDate();	
		default:
			return tempView_employees_jobs.getLastName();
		}
	}



}
