
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.Employee;

	class EmployeeTableModel extends AbstractTableModel {
		public static final int OBJECT_COL = -1;
		private static final int Id_COL = 0;
		private static final int JobsId_COL = 1;
		private static final int FirstName_COL = 2;
		private static final int LastName_COL = 3;
		private static final int Pesel_COL = 4;
		private static final int BirthDate_COL = 5;
		private static final int MonthSalary_COL = 6;
		private static final int EmploymentDate_COL = 7;
		private static final int Mail_COL = 8;
		private static final int PhoneNumber_COL = 9;

//		private int Id;
//		private int JobsId;
//		private String firstName;
//		private String lastName;
//		private BigDecimal Pesel;
//		private Date BirthDate;
//		private BigDecimal MonthSalary;
//		private Date EmploymentDate;
//		private String Mail;
//		private int PhoneNumber;


		private String[] columnNames = {"Id", "JobsId", "firstName", "lastName", "Pesel", "BirthDate",
				"MonthSalary" ,"EmploymentDate" ,"Mail" , "PhoneNumber" };
		private List<Employee> Employee;

		public EmployeeTableModel(List<Employee> theEmployee) {
			Employee = theEmployee;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return Employee.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			Employee tempEmployee = Employee.get(row);

//			private static final int Id_COL = 0;
//			private static final int JobsId_COL = 0;
//
//			private static final int ForeName_COL = 1;
//			private static final int LastName_COL = 2;
//			private static final int Pesel_COL = 3;
//			private static final int BirthDate_COL = 4;
//			private static final int MonthSalary_COL = 5;
//			private static final int EmploymentDate_COL = 6;
//			private static final int Mail_COL = 7;
//			private static final int PhoneNumber_COL = 8;
			switch (col) {
			case Id_COL:
				return tempEmployee.getId();
			case JobsId_COL:
				return tempEmployee.getJobsId();
			case FirstName_COL:
				return tempEmployee.getFirstName();
			case LastName_COL:
				return tempEmployee.getLastName();
			case Pesel_COL:
				return tempEmployee.getPesel();
			case BirthDate_COL:
				return tempEmployee.getBirthDate();
			case MonthSalary_COL:
				return tempEmployee.getMonthSalary();
			case EmploymentDate_COL:
				return tempEmployee.getEmploymentDate();
			case Mail_COL:
				return tempEmployee.getMail();
			case PhoneNumber_COL:
				return tempEmployee.getPhoneNumber();
			case OBJECT_COL:
				return tempEmployee;
			default:
				return tempEmployee.getLastName();
			}
		}



	}
