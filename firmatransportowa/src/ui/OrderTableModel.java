
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.Order;

	class OrderTableModel extends AbstractTableModel {
//		private int I;
//		private int Id;
//		private boolean IfLegalEntity;
//		private int ClientId;
//		private int OrderStateId;
//		private int DestinationLocationId;
//		private int SourceLocationId;
//		private String Description;
//		private Date Deadline;
//		private BigDecimal Price;
//		private Date DepartureDate;

		public static final int OBJECT_COL = -1;
		private static final int Id_COL = 0;
		private static final int IfLegalEntity_COL = 1;
		private static final int ClientId_COL = 2;
		private static final int OrderStateId_COL = 3;
		private static final int DestinationLocationId_COL = 4;
		private static final int SourceLocationId_COL = 5;
		private static final int Description_COL = 6;
		private static final int Deadline_COL = 7;
		private static final int Price_COL = 8;
		private static final int DepartureDate_COL = 9;

//		private int Id;
//		private int JobsId;
//		private String firstName;
//		private String lastName;
//		private BigDecimal Pesel;
//		private Date BirthDate;
//		private BigDecimal MonthSalary;
//		private Date EmploymentDate;
//		private String Mail;
//		private int PhoneNumber;


		private String[] columnNames = {"Id", "IfLegalEntity", "ClientId", "OrderStateId", "DestinationLocationId",
				 "SourceLocationId", "Description", "Deadline", "Price", "DepartureDate" };
		private List<Order> Order;

		public OrderTableModel(List<Order> theOrder) {
			Order = theOrder;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return Order.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			Order tempOrder = Order.get(row);

//			private static final int Id_COL = 0;
//			private static final int JobsId_COL = 0;
//
//			private static final int ForeName_COL = 1;
//			private static final int LastName_COL = 2;
//			private static final int Pesel_COL = 3;
//			private static final int BirthDate_COL = 4;
//			private static final int MonthSalary_COL = 5;
//			private static final int EmploymentDate_COL = 6;
//			private static final int Mail_COL = 7;
//			private static final int PhoneNumber_COL = 8;
			switch (col) {
//			private static final int I_COL = 0;
//			private static final int Id_COL = 0;
//			private static final int IfLegalEntity_COL = 1;
//			private static final int ClientId_COL = 2;
//			private static final int OrderStateId_COL = 3;
//			private static final int DestinationLocationId_COL = 4;
//			private static final int SourceLocationId_COL = 5;
//			private static final int Description_COL = 6;
//			private static final int Deadline_COL = 7;
//			private static final int Price_COL = 8;
//			private static final int DepartureDate_COL = 9;
			
			case Id_COL:
				return tempOrder.getId();
			case IfLegalEntity_COL:
				return tempOrder.getIfLegalEntity();
			case ClientId_COL:
				return tempOrder.getClientId();
			case OrderStateId_COL:
				return tempOrder.getOrderStateId();
			case DestinationLocationId_COL:
				return tempOrder.getDestinationLocationId();
			case SourceLocationId_COL:
				return tempOrder.getSourceLocationId();
			case Description_COL:
				return tempOrder.getDescription();
			case Deadline_COL:
				return tempOrder.getDeadline();
			case Price_COL:
				return tempOrder.getPrice();
			case DepartureDate_COL:
				return tempOrder.getDepartureDate();
			case OBJECT_COL:
				return tempOrder;

				
			default:
				return tempOrder.getId();
			}
		}



	}
