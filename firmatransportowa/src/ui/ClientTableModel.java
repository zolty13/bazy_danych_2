
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.Client;

	class ClientTableModel extends AbstractTableModel {
		public static final int OBJECT_COL = -1;
		private static final int Id_COL = 0;
		private static final int Name_COL = 1;
		private static final int Nip_COL = 2;
		private static final int Regon_COL = 3;
		private static final int LocationId_COL = 4;
		private static final int Mail_COL = 5;
		private static final int PhoneNumber_COL = 6;


//		private int Id;
//		private int JobsId;
//		private String firstName;
//		private String lastName;
//		private BigDecimal Pesel;
//		private Date BirthDate;
//		private BigDecimal MonthSalary;
//		private Date EmploymentDate;
//		private String Mail;
//		private int PhoneNumber;


		private String[] columnNames = {"Id","Name", "Nip", "Regon", "LocationId", "Mail", "PhoneNumber"};
		private List<Client> Client;

		public ClientTableModel(List<Client> theClient) {
			Client = theClient;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return Client.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			Client tempClient = Client.get(row);

//			private static final int Id_COL = 0;
//			private static final int JobsId_COL = 0;
//
//			private static final int ForeName_COL = 1;
//			private static final int LastName_COL = 2;
//			private static final int Pesel_COL = 3;
//			private static final int BirthDate_COL = 4;
//			private static final int MonthSalary_COL = 5;
//			private static final int EmploymentDate_COL = 6;
//			private static final int Mail_COL = 7;
//			private static final int PhoneNumber_COL = 8;
			switch (col) {
//			private static final int Name_COL = 0;
//			private static final int Nip_COL = 1;
//			private static final int Regon_COL = 2;
//			private static final int LocationId_COL = 3;
//			private static final int Mail_COL = 4;
//			private static final int PhoneNumber_COL = 5;
			case Id_COL:
				return tempClient.getId();
			case Name_COL:
				return tempClient.getName();
			case Nip_COL:
				return tempClient.getNip();
			case Regon_COL:
				return tempClient.getRegon();
			case LocationId_COL:
				return tempClient.getLocationId();
			case Mail_COL:
				return tempClient.getMail();
			case PhoneNumber_COL:
				return tempClient.getPhoneNumber();
			case OBJECT_COL:
				return tempClient;
			default:
				return tempClient.getName();
			}
		}



	}
