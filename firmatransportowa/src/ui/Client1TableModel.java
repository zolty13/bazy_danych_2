
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.Client1;

	class Client1TableModel extends AbstractTableModel {
		public static final int OBJECT_COL = -1;
		private static final int Id_COL = 0;
		private static final int ForeName_COL = 1;
		private static final int LastName_COL = 2;
		private static final int Pesel_COL = 3;
		private static final int Nip_COL = 4;
		private static final int LocationId_COL = 5;
		private static final int PhoneNumber_COL = 6;

		private static final int Mail = 7;


//		private int Id;
//		private int JobsId;
//		private String firstName;
//		private String lastName;
//		private BigDecimal Pesel;
//		private Date BirthDate;
//		private BigDecimal MonthSalary;
//		private Date EmploymentDate;
//		private String Mail;
//		private int PhoneNumber;


		private String[] columnNames = {
				"Id","ForeName","LastName", "Pesel", "Nip", "LocationId", "PhoneNumber","Mail"};
		private List<Client1> Client1;

		public Client1TableModel(List<Client1> theClient1) {
			Client1 = theClient1;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return Client1.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			Client1 tempClient1 = Client1.get(row);

//			private static final int Id_COL = 0;
//			private static final int JobsId_COL = 0;
//
//			private static final int ForeName_COL = 1;
//			private static final int LastName_COL = 2;
//			private static final int Pesel_COL = 3;
//			private static final int BirthDate_COL = 4;
//			private static final int MonthSalary_COL = 5;
//			private static final int EmploymentDate_COL = 6;
//			private static final int Mail_COL = 7;
//			private static final int PhoneNumber_COL = 8;
			switch (col) {
//			private static final int ForeName_COL = 0;
//			private static final int LastName_COL = 1;
//
//			private static final int Pesel_COL = 2;
//			private static final int Nip_COL = 3;
//			private static final int LocationId_COL = 4;
//			private static final int Mail = 5;
//			private static final int PhoneNumber_COL = 6;
			case Id_COL:
				return tempClient1.getId();
			case ForeName_COL:
				return tempClient1.getForeName();
			case LastName_COL:
				return tempClient1.getLastName();
			case Pesel_COL:
				return tempClient1.getPesel();
			case Nip_COL:
				return tempClient1.getNip();
			case LocationId_COL:
				return tempClient1.getLocationId();
			case PhoneNumber_COL:
				return tempClient1.getPhoneNumber();
			case Mail:
				return tempClient1.getMail();
			case OBJECT_COL:
				return tempClient1;
			default:
				return tempClient1.getForeName();
			}
		}



	}
