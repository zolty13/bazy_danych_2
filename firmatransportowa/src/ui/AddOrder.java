package ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import core.Employee;
import core.Order;
import dao.View_allclients_locationsDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class AddOrder extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField IfLegalEntitytextField;
	private JTextField ClientIdtextField;
	private JTextField OrderStateIdtextField;
	private JTextField DestinationLocationIdtextField;
	private JTextField SourceLocationIdtextField;
	private JTextField DescriptiontextField;
	private JTextField DeadlinetextField;
	private JTextField PricetextField;
	private JTextField DepartureDatetextField;

	private View_allclients_locationsDAO view_allclients_locationsDAO;
	private View_allclients_locationsSearchApp view_allclients_locationsSearchApp;
	private JTextField IdtextField;
	/**
	 * Launch the application.
	 */	
	
	
	public AddOrder(View_allclients_locationsSearchApp theview_allclients_locationsSearchApp, View_allclients_locationsDAO theview_allclients_locationsDAO) {
			this();
			view_allclients_locationsDAO = theview_allclients_locationsDAO;
			view_allclients_locationsSearchApp = theview_allclients_locationsSearchApp;
		}
	public static void main(String[] args) {
		try {
			AddOrder dialog = new AddOrder();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddOrder() {
		setTitle("Dodaj Zam\u00F3wienie");

		setBounds(100, 100, 463, 321);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[84px][341px,grow]", "[][20px][20px][20px][20px][20px][20px][20px][20px][20px]"));
		{
			JLabel lblId = new JLabel("Id");
			contentPanel.add(lblId, "cell 0 0,alignx trailing");
		}
		{
			IdtextField = new JTextField();
			IdtextField.setColumns(10);
			contentPanel.add(IdtextField, "cell 1 0,growx");
		}
		{
			JLabel lblFirstName = new JLabel("IfLegalEntity");
			contentPanel.add(lblFirstName, "cell 0 1,alignx right,aligny center");
		}
		{
			IfLegalEntitytextField = new JTextField();
			contentPanel.add(IfLegalEntitytextField, "cell 1 1,growx,aligny top");
			IfLegalEntitytextField.setColumns(10);
		}
		{
			JLabel lblFirstName_1 = new JLabel("ClientId");
			contentPanel.add(lblFirstName_1, "cell 0 2,alignx right,aligny center");
		}
		{
			ClientIdtextField = new JTextField();
			contentPanel.add(ClientIdtextField, "cell 1 2,growx,aligny top");
			ClientIdtextField.setColumns(10);
		}
		{
			JLabel lblLastName = new JLabel("OrderStateId");
			contentPanel.add(lblLastName, "cell 0 3,alignx right,aligny center");
		}
		{
			OrderStateIdtextField = new JTextField();
			contentPanel.add(OrderStateIdtextField, "cell 1 3,growx,aligny top");
			OrderStateIdtextField.setColumns(10);
		}
		{
			JLabel lblPesel = new JLabel("DestinationLocationId");
			contentPanel.add(lblPesel, "cell 0 4,alignx right,aligny center");
		}
		{
			DestinationLocationIdtextField = new JTextField();
			contentPanel.add(DestinationLocationIdtextField, "cell 1 4,growx,aligny top");
			DestinationLocationIdtextField.setColumns(10);
		}
		{
			JLabel lblBirthDate = new JLabel("SourceLocationId");
			contentPanel.add(lblBirthDate, "cell 0 5,alignx right,aligny center");
		}
		{
			SourceLocationIdtextField = new JTextField();
			contentPanel.add(SourceLocationIdtextField, "cell 1 5,growx,aligny top");
			SourceLocationIdtextField.setColumns(10);
		}
		{
			JLabel lblMonthSalary = new JLabel("Description");
			contentPanel.add(lblMonthSalary, "cell 0 6,alignx right,aligny center");
		}
		{
			DescriptiontextField = new JTextField();
			contentPanel.add(DescriptiontextField, "cell 1 6,growx,aligny top");
			DescriptiontextField.setColumns(10);
		}
		{
			JLabel lblEmploymentDate = new JLabel("Deadline");
			contentPanel.add(lblEmploymentDate, "cell 0 7,alignx right,aligny center");
		}
		{
			DeadlinetextField = new JTextField();
			contentPanel.add(DeadlinetextField, "cell 1 7,growx,aligny top");
			DeadlinetextField.setColumns(10);
		}
		{
			JLabel lblMail = new JLabel("Price");
			contentPanel.add(lblMail, "cell 0 8,alignx right,aligny center");
		}
		{
			PricetextField = new JTextField();
			contentPanel.add(PricetextField, "cell 1 8,growx,aligny top");
			PricetextField.setColumns(10);
		}
		{
			JLabel lblPhoneNumber = new JLabel("DepartureDate");
			contentPanel.add(lblPhoneNumber, "cell 0 9,alignx right,aligny center");
		}
		{
			DepartureDatetextField = new JTextField();
			DepartureDatetextField.setText("");
			contentPanel.add(DepartureDatetextField, "cell 1 9,growx,aligny top");
			DepartureDatetextField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						saveOrder();
					}

					
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}
	private Order previousOrder = null;
	private boolean updateMode = false;
	public AddOrder(View_allclients_locationsSearchApp theView_allclients_locationsSearchApp,
			View_allclients_locationsDAO theView_allclients_locationsDAO, Order thePreviousOrder, boolean theUpdateMode) {
		this();
		view_allclients_locationsDAO = theView_allclients_locationsDAO;
		view_allclients_locationsSearchApp = theView_allclients_locationsSearchApp;

		previousOrder = thePreviousOrder;
		
		updateMode = theUpdateMode;

		if (updateMode) {
			setTitle("Update Order");
			
			populateGui(previousOrder);
		}
	}
	private void populateGui(Order theOrder) {
	
		IdtextField.setText(Integer.toString(theOrder.getId()));	
		IfLegalEntitytextField.setText(Boolean.toString(theOrder.getIfLegalEntity()));	
		ClientIdtextField.setText(Integer.toString(theOrder.getClientId()));
		OrderStateIdtextField.setText(Integer.toString(theOrder.getOrderStateId()));
		DestinationLocationIdtextField.setText(Integer.toString(theOrder.getDestinationLocationId()));
		SourceLocationIdtextField.setText(Integer.toString(theOrder.getSourceLocationId()));
		DescriptiontextField.setText(theOrder.getDescription());
		DeadlinetextField.setText(theOrder.getDeadline().toString());
		DepartureDatetextField.setText(theOrder.getDepartureDate().toString());
		PricetextField.setText(theOrder.getPrice().toString());
		

	}
	protected BigDecimal convertStringToBigDecimal(String salaryStr) {

		BigDecimal result = null;

		try {
			double salaryDouble = Double.parseDouble(salaryStr);

			result = BigDecimal.valueOf(salaryDouble);
		} catch (Exception exc) {
			System.out.println("Invalid value. Defaulting to 0.0");
			result = BigDecimal.valueOf(0.0);
		}

		return result;
	}
	
	
	
	protected void saveOrder() {
	
		// get the Order info from gui
		String Idstr=IdtextField.getText();
		String IfLegalEntitystr = IfLegalEntitytextField.getText();
		String ClientIdstr = ClientIdtextField.getText();
		String OrderStateIdstr = OrderStateIdtextField.getText();
		String DestinationLocationIdstr = DestinationLocationIdtextField.getText();
		String SourceLocationIdstr = SourceLocationIdtextField.getText();
		String Description = DescriptiontextField.getText();
		String Deadlinestr = DeadlinetextField.getText();
		String DepartureDatestr = DepartureDatetextField.getText();
		String salary = PricetextField.getText();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
		
		java.util.Date textFieldAsDate = null;
		java.util.Date textFieldAsDate1 = null;

		try {
		    textFieldAsDate = sdf.parse(DepartureDatestr);
		} catch (ParseException pe) {
		    // deal with ParseException
		}
		try {
		    textFieldAsDate1 = sdf.parse(Deadlinestr);
		} catch (ParseException pe) {
		    // deal with ParseException
		}
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date DepartureDate = java.sql.Date.valueOf(sdf.format(textFieldAsDate));
		java.sql.Date Deadline = java.sql.Date.valueOf(sdf.format(textFieldAsDate1));

		BigDecimal Price = convertStringToBigDecimal(salary);
		int ClientId = Integer.parseInt(ClientIdstr);
		int OrderStateId = Integer.parseInt(OrderStateIdstr);
		int DestinationLocationId = Integer.parseInt(DestinationLocationIdstr);
		int SourceLocationId = Integer.parseInt(SourceLocationIdstr);
		int Id=Integer.parseInt(Idstr);
		 boolean IfLegalEntity = Boolean.parseBoolean(IfLegalEntitystr);
		 
		Order tempOrder = new Order( Id, IfLegalEntity, ClientId, OrderStateId, DestinationLocationId,
				 SourceLocationId, Description, Deadline, Price, DepartureDate);
		if (updateMode) {
			tempOrder = previousOrder;
			tempOrder.setId(Id);
			tempOrder.setIfLegalEntity(IfLegalEntity);;
			tempOrder.setClientId(ClientId);;
			tempOrder.setOrderStateId(OrderStateId);;
			tempOrder.setDestinationLocationId(DestinationLocationId);;
			tempOrder.setSourceLocationId(SourceLocationId);;
			tempOrder.setDescription(Description);;
			tempOrder.setDeadline(Deadline);;
			tempOrder.setPrice(Price);;
			tempOrder.setDepartureDate(DepartureDate);;
			
		} else {
			tempOrder =new Order( Id, IfLegalEntity, ClientId, OrderStateId, DestinationLocationId,
					 SourceLocationId, Description, Deadline, Price, DepartureDate);
			
		}
		
		try {
			// save to the database
			if (updateMode) {
				view_allclients_locationsDAO.updateOrder(tempOrder);
			} else {
				view_allclients_locationsDAO.addOrder(tempOrder);
				}

			// close dialog
			setVisible(false);
			dispose();

			// refresh gui list
			view_allclients_locationsSearchApp.refreshOrdersView();

			// show success message
			JOptionPane.showMessageDialog(view_allclients_locationsSearchApp,
					"order saved succesfully.", "Order Saved",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(view_allclients_locationsSearchApp,
					"Error saving order: " + exc.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}
}
