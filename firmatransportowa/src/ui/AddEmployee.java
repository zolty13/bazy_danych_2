package ui;

import java.awt.BorderLayout;
import java.awt.FlowLayout;

import javax.swing.JButton;
import javax.swing.JDialog;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import core.Employee;
import dao.View_allclients_locationsDAO;

import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.math.BigDecimal;
import java.sql.Date;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.awt.event.ActionEvent;
import net.miginfocom.swing.MigLayout;

public class AddEmployee extends JDialog {

	private final JPanel contentPanel = new JPanel();
	private JTextField IdtextField;

	private JTextField JobsIdtextField;
	private JTextField FirstNametextField;
	private JTextField LastNametextField;
	private JTextField PeseltextField;
	private JTextField BirthDatetextField;
	private JTextField MonthSalarytextField;
	private JTextField EmploymentDatetextField;
	private JTextField MailtextField;
	private JTextField PhoneNumbertextField;

	private View_allclients_locationsDAO view_allclients_locationsDAO;
	private View_allclients_locationsSearchApp view_allclients_locationsSearchApp;
	/**
	 * Launch the application.
	 */	
	private Employee previousEmployee = null;
	private boolean updateMode = false;

	private int userId;

	public AddEmployee(View_allclients_locationsSearchApp theview_allclients_locationsSearchApp, View_allclients_locationsDAO theview_allclients_locationsDAO
			,Employee thePreviousEmployee, boolean theUpdateMode) {
			this();
			view_allclients_locationsDAO = theview_allclients_locationsDAO;
			view_allclients_locationsSearchApp = theview_allclients_locationsSearchApp;
			previousEmployee = thePreviousEmployee;
			
			updateMode = theUpdateMode;

			
			if (updateMode) {
				setTitle("Update Employee");
				
				populateGui(previousEmployee);
			}
		}
	
	
	
	private void populateGui(Employee theEmployee) {
		IdtextField.setText(Integer.toString(theEmployee.getId()));	
		JobsIdtextField.setText(Integer.toString(theEmployee.getJobsId()));	
		FirstNametextField.setText(theEmployee.getFirstName());
		LastNametextField.setText(theEmployee.getLastName());
		PeseltextField.setText(Long.toString(theEmployee.getPesel()));
		if(theEmployee.getBirthDate().toString()!="0000-00-00")
		BirthDatetextField.setText(theEmployee.getBirthDate().toString());
		MonthSalarytextField.setText(theEmployee.getMonthSalary().toString());
		EmploymentDatetextField.setText(theEmployee.getEmploymentDate().toString());
		MailtextField.setText(theEmployee.getMail());
		PhoneNumbertextField.setText(Integer.toString(theEmployee.getPhoneNumber()));

	}
	
	public static void main(String[] args) {
		try {
			AddEmployee dialog = new AddEmployee();
			dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
			dialog.setVisible(true);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}

	/**
	 * Create the dialog.
	 */
	public AddEmployee(View_allclients_locationsSearchApp view_allclients_locationsSearchApp,View_allclients_locationsDAO view_allclients_locationsDAO) 
	{
		this(view_allclients_locationsSearchApp, view_allclients_locationsDAO, null, false);
	}
	
	public AddEmployee() {
		setTitle("Dodaj Pracownika");

		setBounds(100, 100, 463, 321);
		getContentPane().setLayout(new BorderLayout());
		contentPanel.setBorder(new EmptyBorder(5, 5, 5, 5));
		getContentPane().add(contentPanel, BorderLayout.CENTER);
		contentPanel.setLayout(new MigLayout("", "[84px][341px]", "[][20px][20px][20px][20px][20px][20px][20px][20px][20px]"));
		{
			JLabel lblId = new JLabel("Id");
			contentPanel.add(lblId, "cell 0 0,alignx trailing");
		}
		{
			IdtextField = new JTextField();
			contentPanel.add(IdtextField, "flowx,cell 1 0,growx,aligny top");
			IdtextField.setColumns(10);
		}
		{
			JLabel lblFirstName = new JLabel("Jobs Id");
			contentPanel.add(lblFirstName, "cell 0 1,alignx right,aligny center");
		}
		{
			JobsIdtextField = new JTextField();
			contentPanel.add(JobsIdtextField, "cell 1 1,growx,aligny top");
			JobsIdtextField.setColumns(10);
		}
		{
			JLabel lblFirstName_1 = new JLabel("First Name");
			contentPanel.add(lblFirstName_1, "cell 0 2,alignx right,aligny center");
		}
		{
			FirstNametextField = new JTextField();
			contentPanel.add(FirstNametextField, "cell 1 2,growx,aligny top");
			FirstNametextField.setColumns(10);
		}
		{
			JLabel lblLastName = new JLabel("Last Name");
			contentPanel.add(lblLastName, "cell 0 3,alignx right,aligny center");
		}
		{
			LastNametextField = new JTextField();
			contentPanel.add(LastNametextField, "cell 1 3,growx,aligny top");
			LastNametextField.setColumns(10);
		}
		{
			JLabel lblPesel = new JLabel("Pesel");
			contentPanel.add(lblPesel, "cell 0 4,alignx right,aligny center");
		}
		{
			PeseltextField = new JTextField();
			contentPanel.add(PeseltextField, "cell 1 4,growx,aligny top");
			PeseltextField.setColumns(10);
		}
		{
			JLabel lblBirthDate = new JLabel("Birth Date");
			contentPanel.add(lblBirthDate, "cell 0 5,alignx right,aligny center");
		}
		{
			BirthDatetextField = new JTextField();
			contentPanel.add(BirthDatetextField, "cell 1 5,growx,aligny top");
			BirthDatetextField.setColumns(10);
		}
		{
			JLabel lblMonthSalary = new JLabel("Month Salary");
			contentPanel.add(lblMonthSalary, "cell 0 6,alignx right,aligny center");
		}
		{
			MonthSalarytextField = new JTextField();
			contentPanel.add(MonthSalarytextField, "cell 1 6,growx,aligny top");
			MonthSalarytextField.setColumns(10);
		}
		{
			JLabel lblEmploymentDate = new JLabel("Employment Date");
			contentPanel.add(lblEmploymentDate, "cell 0 7,alignx left,aligny center");
		}
		{
			EmploymentDatetextField = new JTextField();
			contentPanel.add(EmploymentDatetextField, "cell 1 7,growx,aligny top");
			EmploymentDatetextField.setColumns(10);
		}
		{
			JLabel lblMail = new JLabel("Mail");
			contentPanel.add(lblMail, "cell 0 8,alignx right,aligny center");
		}
		{
			MailtextField = new JTextField();
			contentPanel.add(MailtextField, "cell 1 8,growx,aligny top");
			MailtextField.setColumns(10);
		}
		{
			JLabel lblPhoneNumber = new JLabel("Phone Number");
			contentPanel.add(lblPhoneNumber, "cell 0 9,alignx right,aligny center");
		}
		{
			PhoneNumbertextField = new JTextField();
			PhoneNumbertextField.setText("");
			contentPanel.add(PhoneNumbertextField, "cell 1 9,growx,aligny top");
			PhoneNumbertextField.setColumns(10);
		}
		{
			JPanel buttonPane = new JPanel();
			buttonPane.setLayout(new FlowLayout(FlowLayout.RIGHT));
			getContentPane().add(buttonPane, BorderLayout.SOUTH);
			{
				JButton okButton = new JButton("Save");
				okButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						saveEmployee();
					}

					
				});
				okButton.setActionCommand("OK");
				buttonPane.add(okButton);
				getRootPane().setDefaultButton(okButton);
			}
			{
				JButton cancelButton = new JButton("Cancel");
				cancelButton.addActionListener(new ActionListener() {
					public void actionPerformed(ActionEvent arg0) {
						setVisible(false);
						dispose();
					}
				});
				cancelButton.setActionCommand("Cancel");
				buttonPane.add(cancelButton);
			}
		}
	}



	protected BigDecimal convertStringToBigDecimal(String salaryStr) {

		BigDecimal result = null;

		try {
			double salaryDouble = Double.parseDouble(salaryStr);

			result = BigDecimal.valueOf(salaryDouble);
		} catch (Exception exc) {
			System.out.println("Invalid value. Defaulting to 0.0");
			result = BigDecimal.valueOf(0.0);
		}

		return result;
	}
	
	
	
	protected void saveEmployee() {
	
		// get the employee info from gui
		String Idstr = IdtextField.getText();
		String JobsIdstr = JobsIdtextField.getText();
		String firstName = FirstNametextField.getText();
		String lastName = LastNametextField.getText();
		String peselstr = PeseltextField.getText();
		String salaryStr = MonthSalarytextField.getText();
		String mail = MailtextField.getText();
		String PhoneNumberstr = PhoneNumbertextField.getText();
		String BirthDatestr = BirthDatetextField.getText();
		String EmploymentDatestr = EmploymentDatetextField.getText();

		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
		java.util.Date textFieldAsDate = null;
		java.util.Date textFieldAsDate1 = null;

		try {
		    textFieldAsDate = sdf.parse(BirthDatestr);
		} catch (ParseException pe) {
		    // deal with ParseException
		}
		try {
		    textFieldAsDate1 = sdf.parse(EmploymentDatestr);
		} catch (ParseException pe) {
		    // deal with ParseException
		}
		sdf = new SimpleDateFormat("yyyy-MM-dd");
		java.sql.Date BirthDate = java.sql.Date.valueOf(sdf.format(textFieldAsDate));
		java.sql.Date EmploymentDate = java.sql.Date.valueOf(sdf.format(textFieldAsDate1));

		
		
		
		BigDecimal salary = convertStringToBigDecimal(salaryStr);
		int JobsId = Integer.parseInt(JobsIdstr);
		int Id = Integer.parseInt(Idstr);
		int PhoneNumber = Integer.parseInt(PhoneNumberstr);
		long pesel = Long.parseLong(peselstr);
		Employee tempEmployee = null;

		if (updateMode) {
			tempEmployee = previousEmployee;
			tempEmployee.setId(Id);
			tempEmployee.setJobsId(JobsId);
			tempEmployee.setFirstName(firstName);
			tempEmployee.setLastName(lastName);
			tempEmployee.setPesel(pesel);
			tempEmployee.setBirthDate(BirthDate);
			tempEmployee.setMonthSalary(salary);
			tempEmployee.setEmploymentDate(EmploymentDate);
			tempEmployee.setEmail(mail);
			tempEmployee.setPhoneNumber(PhoneNumber);
			
		} else {
			tempEmployee = new Employee(Id, JobsId, firstName, lastName, pesel, BirthDate,salary ,EmploymentDate ,mail , PhoneNumber );
			
		}
		try {
			// save to the database
			if (updateMode) {
				view_allclients_locationsDAO.updateEmployee(tempEmployee);
			} else {
				view_allclients_locationsDAO.addEmployee(tempEmployee);
			}
		
		
			// save to the database

			// close dialog
			setVisible(false);
			dispose();

			// refresh gui list
			view_allclients_locationsSearchApp.refreshEmployeesView();
			
			// show success message
			JOptionPane.showMessageDialog(view_allclients_locationsSearchApp,
					"Employee saved succesfully.", "Employee Saved",
					JOptionPane.INFORMATION_MESSAGE);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(view_allclients_locationsSearchApp,
					"Error saving employee: " + exc.getMessage(), "Error",
					JOptionPane.ERROR_MESSAGE);
		}

	}
}
