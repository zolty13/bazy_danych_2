
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.view_orders_orderstates_viewlocations;

	class view_orders_orderstates_viewlocationsTableModel extends AbstractTableModel {

		private static final int Id_COL = 0;
		private static final int State_COL = 1;
		private static final int IfLegalEntity_COL = 2;
		private static final int ClientId_COL = 3;
		private static final int Deadline_COL = 4;
		private static final int DepartureDate_COL = 5;
		private static final int Description_COL = 6;
		private static final int Price_COL = 7;
		private static final int Street_Dest_COL = 8;
		private static final int Address_Dest_COL = 9;
		private static final int Code_Dest_COL = 10;
		private static final int City_Dest_COL = 11;
		private static final int Country_Dest_COL = 12;
		private static final int Street_Src_COL = 13;
		private static final int Address_Src_COL = 14;
		private static final int Code_Src_COL = 15;
		private static final int City_COL = 16;
		private static final int Country_Src_COL = 17;

	
		private String[] columnNames = { "Id",
				 "State",
				 "IfLegalEntity",
				  "ClientId",
				  "Deadline",
				  "DepartureDate",
				"Description",
				"Price",
				"Code Dest",
				"Address Dest",
				"Code Dest",
				"City Dest",
				 "Country Dest",
				 "Street Src",
				"Address Src",
				 "Code Src",
				 "City Src",
				  "Country Src"};
		private List<view_orders_orderstates_viewlocations> view_orders_orderstates_viewlocations;

		public view_orders_orderstates_viewlocationsTableModel(List<view_orders_orderstates_viewlocations> theview_orders_orderstates_viewlocations) {
			view_orders_orderstates_viewlocations = theview_orders_orderstates_viewlocations;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return view_orders_orderstates_viewlocations.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			view_orders_orderstates_viewlocations tempview_orders_orderstates_viewlocations = view_orders_orderstates_viewlocations.get(row);

			switch (col) {
//			private static final int Id_COL = 0;
//			private static final int State_COL = 1;
//			private static final int IfLegalEntity_COL = 2;
//			private static final int ClientId_COL = 3;
//			private static final int Deadline_COL = 4;
//			private static final int DepartureDate_COL = 5;
//			private static final int Description_COL = 6;
//			private static final int Price_COL = 7;
//			private static final int Street_Dest_COL = 8;
//			private static final int Address_Dest_COL = 9;
//			private static final int Code_Dest_COL = 10;
//			private static final int City_Dest_COL = 11;
//			private static final int Country_Dest_COL = 12;
//			private static final int Street_Src_COL = 13;
//			private static final int Address_Src_COL = 14;
//			private static final int Code_Src_COL = 15;
//			private static final int City_COL = 16;
//			private static final int Country_Src_COL = 17;
			case Id_COL:
				return tempview_orders_orderstates_viewlocations.getId();
			case State_COL:
				return tempview_orders_orderstates_viewlocations.getState();
			case IfLegalEntity_COL:
				return tempview_orders_orderstates_viewlocations.getIfLegalEntity();
			case ClientId_COL:
				return tempview_orders_orderstates_viewlocations.getClientId();
			case Deadline_COL:
				return tempview_orders_orderstates_viewlocations.getDeadline();
			case DepartureDate_COL:
				return tempview_orders_orderstates_viewlocations.getDepartureDate();
			case Description_COL:
				return tempview_orders_orderstates_viewlocations.getDescription();
			case Price_COL:
				return tempview_orders_orderstates_viewlocations.getPrice();
			case Street_Dest_COL:
				return tempview_orders_orderstates_viewlocations.getStreet_Dest();
			case Address_Dest_COL:
				return tempview_orders_orderstates_viewlocations.getAddress_Dest();
			case Code_Dest_COL:
				return tempview_orders_orderstates_viewlocations.getCode_Dest();
			case City_Dest_COL:
				return tempview_orders_orderstates_viewlocations.getCity_Dest();
			case Country_Dest_COL:
				return tempview_orders_orderstates_viewlocations.getCountry_Dest();
			case Street_Src_COL:
				return tempview_orders_orderstates_viewlocations.getStreet_Src();
			case Address_Src_COL:
				return tempview_orders_orderstates_viewlocations.getAddress_Src();
			case Code_Src_COL:
				return tempview_orders_orderstates_viewlocations.getCode_Src();
			case City_COL:
				return tempview_orders_orderstates_viewlocations.getCity_Src();
			case Country_Src_COL:
				return tempview_orders_orderstates_viewlocations.getCountry_Src();
			default:
				return tempview_orders_orderstates_viewlocations.getId();
			}
		}



	}

