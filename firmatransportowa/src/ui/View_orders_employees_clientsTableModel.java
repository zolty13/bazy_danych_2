
	package ui;

	import java.math.BigDecimal;
import java.sql.Date;
import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.View_orders_employees_clients;

	class View_orders_employees_clientsTableModel extends AbstractTableModel {

		private static final int Orderid_COL = 0;
		private static final int State_COL = 1;
		private static final int IfLegalEntity_COL = 2;
		private static final int ClientId_COL = 3;
		private static final int ForeName_COL = 4;
		private static final int Company_LastName_COL = 5;
		private static final int Pesel_Regon_COL = 6;
		private static final int Cmail_COL = 7;
		private static final int CPhone_COL = 8;
		private static final int DestinationLocationID_COL = 9;
		private static final int SourceLocationID_COL = 10;
		private static final int Description_COL = 11;
		private static final int Deadline_COL = 12;
		private static final int Price_COL = 13;
		private static final int DepartureDate_COL = 14;
		private static final int EmpId_COL = 15;
		private static final int FirstName_COL = 16;
		private static final int LastName_COL = 17;
		private static final int Pesel_COL = 18;
		private static final int Mail_COL = 19;
		private static final int PhoneNumber_COL = 20;
	
		private String[] columnNames = { "Orderid", "State", "IfLegalEntity",
				"ClientId", "ForeName", "Company_LastName", "Pesel_Regon", "Cmail", "CPhone", "DestinationLocationID", "SourceLocationID", "Description",
				"Deadline", "Price", "DepartureDate", "EmpId", "FirstName", "LastName", "Pesel", "Mail", "PhoneNumber"};
		private List<View_orders_employees_clients> View_orders_employees_clients;

		public View_orders_employees_clientsTableModel(List<View_orders_employees_clients> theView_orders_employees_clients) {
			View_orders_employees_clients = theView_orders_employees_clients;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return View_orders_employees_clients.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			View_orders_employees_clients tempView_orders_employees_clients = View_orders_employees_clients.get(row);

			switch (col) {
//			private static final int Orderid_COL = 0;
//			private static final int State_COL = 1;
//			private static final int IfLegalEntity_COL = 2;
//			private static final int ClientId_COL = 3;
//			private static final int ForeName_COL = 4;
//			private static final int Company_LastName_COL = 5;
//			private static final int Pesel_Regon_COL = 6;
//			private static final int Cmail_COL = 7;
//			private static final int CPhone_COL = 8;
//			private static final int DestinationLocationID_COL = 9;
//			private static final int SourceLocationID_COL = 9;
//			private static final int Description_COL = 10;
//			private static final int Deadline_COL = 11;
//			private static final int Price_COL = 12;
//			private static final int DepartureDate_COL = 13;
//			private static final int EmpId_COL = 14;
//			private static final int FirstName_COL = 15;
//			private static final int LastName_COL = 16;
//			private static final int Pesel_COL = 17;
//			private static final int Mail_COL = 18;
//			private static final int PhoneNumber_COL = 19;
			case Orderid_COL:
				return tempView_orders_employees_clients.getOrderid();
			case State_COL:
				return tempView_orders_employees_clients.getState();
			case IfLegalEntity_COL:
				return tempView_orders_employees_clients.getIfLegalEntity();
			case ClientId_COL:
				return tempView_orders_employees_clients.getClientId();
			case ForeName_COL:
				return tempView_orders_employees_clients.getForeName();
			case Company_LastName_COL:
				return tempView_orders_employees_clients.getCompany_LastName();
			case Pesel_Regon_COL:
				return tempView_orders_employees_clients.getPesel_Regon();
			case Cmail_COL:
				return tempView_orders_employees_clients.getCmail();
			case CPhone_COL:
				return tempView_orders_employees_clients.getCPhone();
			case DestinationLocationID_COL:
				return tempView_orders_employees_clients.getDestinationLocationID();
			case SourceLocationID_COL:
				return tempView_orders_employees_clients.getSourceLocationID();
			case Description_COL:
				return tempView_orders_employees_clients.getDescription();
			case Deadline_COL:
				return tempView_orders_employees_clients.getDeadline();
			case Price_COL:
				return tempView_orders_employees_clients.getPrice();
			case DepartureDate_COL:
				return tempView_orders_employees_clients.getDepartureDate();
			case EmpId_COL:
				return tempView_orders_employees_clients.getEmpId();
			case FirstName_COL:
				return tempView_orders_employees_clients.getFirstName();
			case LastName_COL:
				return tempView_orders_employees_clients.getLastName();
			case Pesel_COL:
				return tempView_orders_employees_clients.getPesel();
			case Mail_COL:
				return tempView_orders_employees_clients.getMail();
			case PhoneNumber_COL:
				return tempView_orders_employees_clients.getPhoneNumber();
			default:
				return tempView_orders_employees_clients.getOrderid();
			}
		}



	}

