package ui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JLabel;
import java.awt.FlowLayout;
import javax.swing.JTextField;
import javax.swing.JButton;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

import javax.swing.JOptionPane;
import java.util.List;

import core.Client;
import core.Client1;
import core.Employee;
import core.Order;
import core.View_allclients_locations;
import core.View_employees_jobs;
import core.View_entitylegalclients_viewlocationscitiescountries;
import core.View_naturalpersonclients_viewlocationscitiescountries;
import core.View_orders_employees_clients;
import core.view_orders_orderstates_viewlocations;
import core.View_locations_cities_countries;
import dao.View_allclients_locationsDAO;
import dao.View_employees_jobsDAO;

import javax.swing.JList;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.SwingConstants;
import javax.swing.JMenuItem;
import java.awt.Button;

public class View_allclients_locationsSearchApp extends JFrame {

	private JPanel contentPane;
	private JTextField Company_LastNameTextField;
	private JScrollPane scrollPane;
	private JTable table;

	private View_allclients_locationsDAO view_allclients_locationsDAO;
	
	private View_employees_jobsDAO view_employees_jobsDAO;

	private JMenuBar menuBar;
	private JMenu mnWidoki;
	private JMenuItem btnSearch;
	private JMenuItem mntmNewMenuItem;
	private JMenuItem mntmNewMenuItem_1;
	private JMenuItem mntmNewMenuItem_2;
	private JMenuItem mntmNewMenuItem_3;
	private JMenuItem mntmNewMenuItem_4;
	private JMenuItem mntmNewMenuItem_5;
	private JTextField LastNameTextField;
	private JMenu mnDodaj;
	private JMenuItem btnAddEmployee;
	private JMenuItem dodaj2;
	private JMenuItem mntmDodajentitylegalclients;
	private JMenuItem mntmDodajZleceniuKierowce;
	private JMenu mnTabela;
	private JMenuItem mntmPracownicy;
	private JMenuItem mntmZamowienia;
	private JMenuItem mntmEntitylegalclients;
	private JMenuItem mntmNaturalpersonclients;
	private JPanel panel_1;
	private JMenu mnUpdate;
	private JMenuItem mntmUpdatePracownika;
	private JMenuItem mntmUpdateZamwienie;
	private JMenuItem mntmUpdateEntitylegalclients;
	private JMenuItem mntmUpdateNaturalpersonclients;
	private JMenu mnDelete;
	private JMenuItem mntmDeletePracownika;
	private JMenuItem mntmDeleteZamwienia;
	private JMenuItem mntmDeleteEntitylegalclients;
	private JMenuItem mntmDeleteNaturalpersonclients;
	/**
	 * Launch the application.
	 */
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					View_allclients_locationsSearchApp frame = new View_allclients_locationsSearchApp();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public View_allclients_locationsSearchApp() {
		
		// create the DAO
		try {
			view_allclients_locationsDAO = new View_allclients_locationsDAO();
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
		}
		
		setTitle("view_allclients_locations Search App");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(100, 100, 780, 532);
		
		menuBar = new JMenuBar();
		setJMenuBar(menuBar);
		
		mnTabela = new JMenu("Tabela");
		menuBar.add(mnTabela);
		
		mntmPracownicy = new JMenuItem("Pracownicy");
		mntmPracownicy.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<Employee> Employee = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					Employee = view_allclients_locationsDAO.searchEmployees(Company_LastName);
					} else {
						Employee = view_allclients_locationsDAO.getAllEmployees();
					}
					
					// create the model and update the "table"
					EmployeeTableModel model = new EmployeeTableModel(Employee);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnTabela.add(mntmPracownicy);
		
		mntmZamowienia = new JMenuItem("Zamowienia");
		mntmZamowienia.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<Order> Order = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					Order = view_allclients_locationsDAO.searchOrders(Company_LastName);
					} else {
						Order = view_allclients_locationsDAO.getAllOrders();
					}
					
					// create the model and update the "table"
					OrderTableModel model = new OrderTableModel(Order);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnTabela.add(mntmZamowienia);
		
		mntmEntitylegalclients = new JMenuItem("EntityLegalClients");
		mntmEntitylegalclients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<Client> Client = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					Client = view_allclients_locationsDAO.searchClients(Company_LastName);
					} else {
						Client = view_allclients_locationsDAO.getAllClients();
					}
					
					// create the model and update the "table"
					ClientTableModel model = new ClientTableModel(Client);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnTabela.add(mntmEntitylegalclients);
		
		mntmNaturalpersonclients = new JMenuItem("naturalpersonclients");
		mntmNaturalpersonclients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allClient1s_locationss for the last name

				// If last name is empty, then get all view_allClient1s_locationss

				// Print out view_allClient1s_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<Client1> Client1 = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					Client1 = view_allclients_locationsDAO.searchClient1s(Company_LastName);
					} else {
						Client1 = view_allclients_locationsDAO.getAllClient1s();
					}
					
					// create the model and update the "table"
					Client1TableModel model = new Client1TableModel(Client1);
					
					table.setModel(model);
					
					/*
					for (view_allClient1s_locations temp : view_allClient1s_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnTabela.add(mntmNaturalpersonclients);
		
		mnWidoki = new JMenu("Widoki");
		menuBar.add(mnWidoki);
		
		btnSearch = new JMenuItem("view_allclients_locations");
		mnWidoki.add(btnSearch);
		btnSearch.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<View_allclients_locations> view_allclients_locations = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
						view_allclients_locations = view_allclients_locationsDAO.searchView_allclients_locations(Company_LastName);
					} else {
						view_allclients_locations = view_allclients_locationsDAO.getAllView_allclients_locations();
					}
					
					// create the model and update the "table"
					View_allclients_locationsTableModel model = new View_allclients_locationsTableModel(view_allclients_locations);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		
		mntmNewMenuItem = new JMenuItem("view_employees_jobs");
		mnWidoki.add(mntmNewMenuItem);
		mntmNewMenuItem.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<View_employees_jobs> view_employees_jobs = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					view_employees_jobs = view_allclients_locationsDAO.searchView_employees_jobs(Company_LastName);
					} else {
						view_employees_jobs = view_allclients_locationsDAO.getAllView_employees_jobs();
					}
					
					// create the model and update the "table"
					View_employees_jobsTableModel model = new View_employees_jobsTableModel(view_employees_jobs);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		
		mntmNewMenuItem_1 = new JMenuItem("view_entitylegalclients_viewlocationscitiescountries");
		mntmNewMenuItem_1.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<View_entitylegalclients_viewlocationscitiescountries> View_entitylegalclients_viewlocationscitiescountries = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					} else {
						View_entitylegalclients_viewlocationscitiescountries = view_allclients_locationsDAO.getAllView_entitylegalclients_viewlocationscitiescountries();
					}
					
					// create the model and update the "table"
					View_entitylegalclients_viewlocationscitiescountriesTableModel model = new View_entitylegalclients_viewlocationscitiescountriesTableModel(View_entitylegalclients_viewlocationscitiescountries);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnWidoki.add(mntmNewMenuItem_1);
		
		mntmNewMenuItem_2 = new JMenuItem("view_locations_cities_countries");
		mntmNewMenuItem_2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<View_locations_cities_countries> view_locations_cities_countries = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					view_locations_cities_countries = view_allclients_locationsDAO.searchview_locations_cities_countries(Company_LastName);
					} else {
						view_locations_cities_countries = view_allclients_locationsDAO.getAllview_locations_cities_countries();
					}
					
					// create the model and update the "table"
					View_locations_cities_countriesTableModel model = new View_locations_cities_countriesTableModel(view_locations_cities_countries);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnWidoki.add(mntmNewMenuItem_2);
		
		mntmNewMenuItem_3 = new JMenuItem("view_naturalpersonclients_viewlocationscitiescountries");
		mntmNewMenuItem_3.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get View_naturalpersonclients_viewlocationscitiescountriess for the last name

				// If last name is empty, then get all View_naturalpersonclients_viewlocationscitiescountriess

				// Print out View_naturalpersonclients_viewlocationscitiescountriess				
				
				try {
					String LastName = Company_LastNameTextField.getText();

					List<View_naturalpersonclients_viewlocationscitiescountries> View_naturalpersonclients_viewlocationscitiescountries = null;

					if (LastName != null && LastName.trim().length() > 0) {
						View_naturalpersonclients_viewlocationscitiescountries = view_allclients_locationsDAO.searchView_naturalpersonclients_viewlocationscitiescountries(LastName);
					} else {
						View_naturalpersonclients_viewlocationscitiescountries = view_allclients_locationsDAO.getAllView_naturalpersonclients_viewlocationscitiescountries();
					}
					
					// create the model and update the "table"
					View_naturalpersonclients_viewlocationscitiescountriesTableModel model = new View_naturalpersonclients_viewlocationscitiescountriesTableModel(View_naturalpersonclients_viewlocationscitiescountries);
					
					table.setModel(model);
					
					/*
					for (View_naturalpersonclients_viewlocationscitiescountries temp : View_naturalpersonclients_viewlocationscitiescountriess) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnWidoki.add(mntmNewMenuItem_3);
		
		mntmNewMenuItem_4 = new JMenuItem("view_orders_employees_clients");
		mntmNewMenuItem_4.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<View_orders_employees_clients> View_orders_employees_clients = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					View_orders_employees_clients = view_allclients_locationsDAO.searchView_orders_employees_clients(Company_LastName);
					} else {
						View_orders_employees_clients = view_allclients_locationsDAO.getAllView_orders_employees_clients();
					}
					
					// create the model and update the "table"
					View_orders_employees_clientsTableModel model = new View_orders_employees_clientsTableModel(View_orders_employees_clients);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnWidoki.add(mntmNewMenuItem_4);
		
		mntmNewMenuItem_5 = new JMenuItem("view_orders_orderstates_viewlocations");
		mntmNewMenuItem_5.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				// Get last name from the text field

				// Call DAO and get view_allclients_locationss for the last name

				// If last name is empty, then get all view_allclients_locationss

				// Print out view_allclients_locationss				
				
				try {
					String Company_LastName = Company_LastNameTextField.getText();

					List<view_orders_orderstates_viewlocations> view_orders_orderstates_viewlocations = null;

					if (Company_LastName != null && Company_LastName.trim().length() > 0) {
					view_orders_orderstates_viewlocations = view_allclients_locationsDAO.searchview_orders_orderstates_viewlocations(Company_LastName);
					} else {
						view_orders_orderstates_viewlocations = view_allclients_locationsDAO.getAllview_orders_orderstates_viewlocations();
					}
					
					// create the model and update the "table"
					view_orders_orderstates_viewlocationsTableModel model = new view_orders_orderstates_viewlocationsTableModel(view_orders_orderstates_viewlocations);
					
					table.setModel(model);
					
					/*
					for (view_allclients_locations temp : view_allclients_locationss) {
						System.out.println(temp);
					}
					*/
				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "Error: " + exc, "Error", JOptionPane.ERROR_MESSAGE); 
				}
				
			}
		});
		mnWidoki.add(mntmNewMenuItem_5);
		
		mnDodaj = new JMenu("Dodaj");
		menuBar.add(mnDodaj);
		
		btnAddEmployee = new JMenuItem("Dodaj Pracownika");
		btnAddEmployee.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddEmployee dialog = new AddEmployee(View_allclients_locationsSearchApp.this, view_allclients_locationsDAO);
				

				
				dialog.setVisible(true);
			}
		});
		mnDodaj.add(btnAddEmployee);
		
		dodaj2 = new JMenuItem("Dodaj Zam\u00F3wienie");
		dodaj2.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddOrder dialog = new AddOrder(View_allclients_locationsSearchApp.this,view_allclients_locationsDAO);

				
				dialog.setVisible(true);
			}
		});
		mnDodaj.add(dodaj2);
		
		mntmDodajentitylegalclients = new JMenuItem("Dodaj entitylegalclients");
		mntmDodajentitylegalclients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddClient dialog = new AddClient(View_allclients_locationsSearchApp.this,view_allclients_locationsDAO);

				
				dialog.setVisible(true);
			}
		});
		mnDodaj.add(mntmDodajentitylegalclients);
		
		mntmDodajZleceniuKierowce = new JMenuItem("Dodaj naturalpersonclients");
		mntmDodajZleceniuKierowce.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				AddClient1 dialog = new AddClient1(View_allclients_locationsSearchApp.this,view_allclients_locationsDAO);

				
				dialog.setVisible(true);
			}
		});
		mnDodaj.add(mntmDodajZleceniuKierowce);
		
		mnUpdate = new JMenu("Update");
		menuBar.add(mnUpdate);
		
		mntmUpdatePracownika = new JMenuItem("Update Pracownika");
		mntmUpdatePracownika.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {

				// get the selected item
				int row = table.getSelectedRow();
				
				// make sure a row is selected
				if (row < 0) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "You must select an employee", "Error",
							JOptionPane.ERROR_MESSAGE);				
					return;
				}
				
				// get the current employee
				Employee tempEmployee = (Employee) table.getValueAt(row, EmployeeTableModel.OBJECT_COL);
	
				// create dialog
				AddEmployee dialog = new AddEmployee(View_allclients_locationsSearchApp.this, view_allclients_locationsDAO, 
															tempEmployee, true);

				// show dialog
				dialog.setVisible(true);
			
			}
		}
		);
		mnUpdate.add(mntmUpdatePracownika);
		
		mntmUpdateZamwienie = new JMenuItem("Update Zam\u00F3wienia");
		mntmUpdateZamwienie.addActionListener(new ActionListener() {
				public void actionPerformed(ActionEvent e) {

				// get the selected item
				int row = table.getSelectedRow();
				
				// make sure a row is selected
				if (row < 0) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, "You must select an order", "Error",
							JOptionPane.ERROR_MESSAGE);				
					return;
				}
				
				// get the current employee
				Order tempOrder = (Order) table.getValueAt(row, EmployeeTableModel.OBJECT_COL);
	
				// create dialog
				AddOrder dialog = new AddOrder(View_allclients_locationsSearchApp.this, view_allclients_locationsDAO, 
															tempOrder, true);

				// show dialog
				dialog.setVisible(true);
			
			}
		});
		mnUpdate.add(mntmUpdateZamwienie);
		
		mntmUpdateEntitylegalclients = new JMenuItem("Update entitylegalclients");
		mnUpdate.add(mntmUpdateEntitylegalclients);
		
		mntmUpdateNaturalpersonclients = new JMenuItem("Update naturalpersonclients");
		mnUpdate.add(mntmUpdateNaturalpersonclients);
		
		mnDelete = new JMenu("delete");
		menuBar.add(mnDelete);
		
		mntmDeletePracownika = new JMenuItem("delete Pracownika");
		mntmDeletePracownika.addActionListener(new ActionListener() {
		public void actionPerformed(ActionEvent e) {
				
				try {
					// get the selected row
					int row = table.getSelectedRow();

					// make sure a row is selected
					if (row < 0) {
						JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, 
								"You must select an employee", "Error", JOptionPane.ERROR_MESSAGE);				
						return;
					}

					// prompt the user
					int response = JOptionPane.showConfirmDialog(
							View_allclients_locationsSearchApp.this, "Delete this employee?", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response != JOptionPane.YES_OPTION) {
						return;
					}

					// get the current employee
					Employee tempEmployee = (Employee) table.getValueAt(row, EmployeeTableModel.OBJECT_COL);

					// delete the employee
					view_allclients_locationsDAO.deleteEmployee(tempEmployee.getId());

					// refresh GUI
					refreshEmployeesView();

					// show success message
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Employee deleted succesfully.", "Employee Deleted",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Error deleting employee: " + exc.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}

				
			}
		});
		mnDelete.add(mntmDeletePracownika);
		
		mntmDeleteZamwienia = new JMenuItem("delete Zam\u00F3wienia");
		mntmDeleteZamwienia.addActionListener(new ActionListener() {
	public void actionPerformed(ActionEvent e) {
				
				try {
					// get the selected row
					int row = table.getSelectedRow();

					// make sure a row is selected
					if (row < 0) {
						JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, 
								"You must select an order", "Error", JOptionPane.ERROR_MESSAGE);				
						return;
					}

					// prompt the user
					int response = JOptionPane.showConfirmDialog(
							View_allclients_locationsSearchApp.this, "Delete this order?", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response != JOptionPane.YES_OPTION) {
						return;
					}

					// get the current employee
					Order tempOrder = (Order) table.getValueAt(row, OrderTableModel.OBJECT_COL);

					// delete the employee
					view_allclients_locationsDAO.deleteOrder(tempOrder.getId());

					// refresh GUI
					refreshEmployeesView();

					// show success message
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Order deleted succesfully.", "Order Deleted",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Error deleting Order: " + exc.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}

				
			}
		});
		mnDelete.add(mntmDeleteZamwienia);
		
		mntmDeleteEntitylegalclients = new JMenuItem("delete entitylegalclients");
		mntmDeleteEntitylegalclients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					// get the selected row
					int row = table.getSelectedRow();

					// make sure a row is selected
					if (row < 0) {
						JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, 
								"You must select an Client", "Error", JOptionPane.ERROR_MESSAGE);				
						return;
					}

					// prompt the user
					int response = JOptionPane.showConfirmDialog(
							View_allclients_locationsSearchApp.this, "Delete this client?", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response != JOptionPane.YES_OPTION) {
						return;
					}

					// get the current employee
					Client tempClient = (Client) table.getValueAt(row, ClientTableModel.OBJECT_COL);

					// delete the employee
					view_allclients_locationsDAO.deleteClient1(tempClient.getId());

					// refresh GUI
					refreshClient1sView();

					// show success message
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Client deleted succesfully.", "Client Deleted",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Error deleting Client: " + exc.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}

				
			}
		});
		mnDelete.add(mntmDeleteEntitylegalclients);
		
		mntmDeleteNaturalpersonclients = new JMenuItem("delete naturalpersonclients");
		mntmDeleteNaturalpersonclients.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				
				try {
					// get the selected row
					int row = table.getSelectedRow();

					// make sure a row is selected
					if (row < 0) {
						JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this, 
								"You must select an Client", "Error", JOptionPane.ERROR_MESSAGE);				
						return;
					}

					// prompt the user
					int response = JOptionPane.showConfirmDialog(
							View_allclients_locationsSearchApp.this, "Delete this client?", "Confirm", 
							JOptionPane.YES_NO_OPTION, JOptionPane.QUESTION_MESSAGE);

					if (response != JOptionPane.YES_OPTION) {
						return;
					}

					// get the current employee
					Client1 tempClient1 = (Client1) table.getValueAt(row, Client1TableModel.OBJECT_COL);

					// delete the employee
					view_allclients_locationsDAO.deleteClient1(tempClient1.getId());

					// refresh GUI
					refreshClient1sView();

					// show success message
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Client deleted succesfully.", "Client Deleted",
							JOptionPane.INFORMATION_MESSAGE);

				} catch (Exception exc) {
					JOptionPane.showMessageDialog(View_allclients_locationsSearchApp.this,
							"Error deleting Client: " + exc.getMessage(), "Error",
							JOptionPane.ERROR_MESSAGE);
				}

				
			}
		});
		mnDelete.add(mntmDeleteNaturalpersonclients);

		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		
		JPanel panel = new JPanel();
		FlowLayout flowLayout = (FlowLayout) panel.getLayout();
		flowLayout.setAlignment(FlowLayout.LEFT);
		contentPane.add(panel, BorderLayout.NORTH);
		
		JLabel lblEnterCompany_LastName = new JLabel("Wyszukaj");
		panel.add(lblEnterCompany_LastName);
		
		Company_LastNameTextField = new JTextField();
		panel.add(Company_LastNameTextField);
		Company_LastNameTextField.setColumns(10);
		
		LastNameTextField = new JTextField();
		LastNameTextField.setColumns(10);
		panel.add(LastNameTextField);
		
		scrollPane = new JScrollPane();
		contentPane.add(scrollPane, BorderLayout.CENTER);
		
		table = new JTable();
		scrollPane.setViewportView(table);
		
		panel_1 = new JPanel();
		contentPane.add(panel_1, BorderLayout.SOUTH);
	}
	public void refreshEmployeesView() {

		try {
			List<Employee> employees = view_allclients_locationsDAO.getAllEmployees();

			// create the model and update the "table"
			EmployeeTableModel model = new EmployeeTableModel(employees);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
	public void refreshOrdersView() {

		try {
			List<Order> Order = view_allclients_locationsDAO.getAllOrders();

			// create the model and update the "table"
			OrderTableModel model = new OrderTableModel(Order);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
	public void refreshClientsView() {

		try {
			List<Client> Client = view_allclients_locationsDAO.getAllClients();

			// create the model and update the "table"
			ClientTableModel model = new ClientTableModel(Client);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
	public void refreshClient1sView() {

		try {
			List<Client1> Client1 = view_allclients_locationsDAO.getAllClient1s();

			// create the model and update the "table"
			Client1TableModel model = new Client1TableModel(Client1);

			table.setModel(model);
		} catch (Exception exc) {
			JOptionPane.showMessageDialog(this, "Error: " + exc, "Error",
					JOptionPane.ERROR_MESSAGE);
		}
		
	}
}
