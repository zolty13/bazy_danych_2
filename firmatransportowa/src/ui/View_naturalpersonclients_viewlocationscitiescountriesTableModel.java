	package ui;

	import java.util.List;

	import javax.swing.table.AbstractTableModel;



	import core.View_naturalpersonclients_viewlocationscitiescountries;

	class View_naturalpersonclients_viewlocationscitiescountriesTableModel extends AbstractTableModel {

		private static final int Id_COL = 0;
		private static final int ForeName_COL = 1;
		private static final int LastName_COL = 2;
		private static final int Pesel_COL = 3;
		private static final int NIP_COL = 4;
		private static final int Mail_COL = 5;
		private static final int PhoneNumber_COL = 6;
		private static final int StreetAddressLine1_COL = 7;
		private static final int StreetAddressLine2_NAME_COL = 8;
		private static final int PostalCode_COL = 9;
		private static final int CityName_COL = 10;
		private static final int CountryName_COL = 11;


		private String[] columnNames = { "id", "ForeName", "LastName",
				"Pesel", "NIP", "Mail", "PhoneNumber", "StreetAddressLine1", "StreetAddressLine2", "PostalCode", "CityName", "CountryName"};
		private List<View_naturalpersonclients_viewlocationscitiescountries> View_naturalpersonclients_viewlocationscitiescountries;

		public View_naturalpersonclients_viewlocationscitiescountriesTableModel(List<View_naturalpersonclients_viewlocationscitiescountries> theView_naturalpersonclients_viewlocationscitiescountries) {
			View_naturalpersonclients_viewlocationscitiescountries = theView_naturalpersonclients_viewlocationscitiescountries;
		}

		@Override
		public int getColumnCount() {
			return columnNames.length;
		}

		@Override
		public int getRowCount() {
			return View_naturalpersonclients_viewlocationscitiescountries.size();
		}

		@Override
		public String getColumnName(int col) {
			return columnNames[col];
		}

		@Override
		public Object getValueAt(int row, int col) {

			View_naturalpersonclients_viewlocationscitiescountries tempView_naturalpersonclients_viewlocationscitiescountries = View_naturalpersonclients_viewlocationscitiescountries.get(row);

			switch (col) {
			case Id_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getId();
			case ForeName_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getForeName();
			case LastName_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getLastName();
			case Pesel_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getPesel();
			case NIP_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getNIP();
			case Mail_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getMail();
			case PhoneNumber_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getPhoneNumber();
			case StreetAddressLine1_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getStreetAddressLine1();
			case StreetAddressLine2_NAME_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getStreetAddressLine2();
			case PostalCode_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getPostalCode();
			case CityName_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getCityName();
			case CountryName_COL:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getCountryName();

				
			default:
				return tempView_naturalpersonclients_viewlocationscitiescountries.getForeName();
			}
		}



	}

