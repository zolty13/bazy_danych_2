-- phpMyAdmin SQL Dump
-- version 4.7.4
-- https://www.phpmyadmin.net/
--
-- Host: 127.0.0.1
-- Czas generowania: 19 Gru 2017, 19:02
-- Wersja serwera: 10.1.29-MariaDB
-- Wersja PHP: 7.1.12

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET AUTOCOMMIT = 0;
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Baza danych: `transportcompany`
--

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `cities`
--

CREATE TABLE `cities` (
  `Id` int(11) UNSIGNED NOT NULL,
  `CityName` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `CountryId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `cities`
--

INSERT INTO `cities` (`Id`, `CityName`, `CountryId`) VALUES
(1, 'Żory', 1),
(2, 'Berlin', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `countries`
--

CREATE TABLE `countries` (
  `Id` int(11) UNSIGNED NOT NULL,
  `CountryName` varchar(32) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `countries`
--

INSERT INTO `countries` (`Id`, `CountryName`) VALUES
(1, 'Polska'),
(2, 'RFN');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `employees`
--

CREATE TABLE `employees` (
  `Id` int(11) UNSIGNED NOT NULL,
  `JobsId` int(11) UNSIGNED NOT NULL,
  `FirstName` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `LastName` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `Pesel` varchar(11) COLLATE utf8_polish_ci NOT NULL,
  `BirthDate` date DEFAULT NULL,
  `MonthSalary` float DEFAULT NULL,
  `EmploymentDate` date DEFAULT NULL,
  `Mail` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `PhoneNumber` varchar(16) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `employees`
--

INSERT INTO `employees` (`Id`, `JobsId`, `FirstName`, `LastName`, `Pesel`, `BirthDate`, `MonthSalary`, `EmploymentDate`, `Mail`, `PhoneNumber`) VALUES
(1, 1, 'Paweł Jan', 'Żółtaniecki', '9654334567', '0000-00-00', 15000, '0000-00-00', 'zolt@pp.pl', '765876777'),
(2, 3, 'Piotr', 'Żółtaniecki', '9654336567', '0000-00-00', 15000, '0000-00-00', 'zollooo@pp.pl', '765876733'),
(3, 2, 'Paweł', 'Kowal', '9654334555', '0000-00-00', 5000, '0000-00-00', 'kow@pp.pl', '335876777'),
(5, 2, 'Paweł', 'Kowalski', '965433333', '0000-00-00', 5000, '0000-00-00', 'kow@pp.pl', '335876777'),
(7, 3, 'Michał', 'Graca', '7787887654', '0000-00-00', 3900, '0000-00-00', 'graca@op.pl', '123456789'),
(8, 2, 'Damian', 'Pepek', '98887676777', NULL, NULL, NULL, 'dama@wp.pl', '123476578');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `entitylegalclients`
--

CREATE TABLE `entitylegalclients` (
  `Id` int(11) UNSIGNED NOT NULL,
  `Name` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `Regon` varchar(14) COLLATE utf8_polish_ci NOT NULL,
  `LocationId` int(11) UNSIGNED NOT NULL,
  `Mail` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `PhoneNumber` varchar(15) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `entitylegalclients`
--

INSERT INTO `entitylegalclients` (`Id`, `Name`, `NIP`, `Regon`, `LocationId`, `Mail`, `PhoneNumber`) VALUES
(1, 'Mix', NULL, '12345678900', 1, 'mix@op.pl', '1237789444'),
(2, 'bayern', NULL, '66645678900', 2, 'bayern@op.pl', '777789444');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `jobs`
--

CREATE TABLE `jobs` (
  `Id` int(11) UNSIGNED NOT NULL,
  `JobName` varchar(8) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `jobs`
--

INSERT INTO `jobs` (`Id`, `JobName`) VALUES
(2, 'Boss'),
(3, 'ClientDe'),
(1, 'Driver');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `locations`
--

CREATE TABLE `locations` (
  `Id` int(11) UNSIGNED NOT NULL,
  `StreetAddressLine1` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `StreetAddressLine2` varchar(8) COLLATE utf8_polish_ci NOT NULL,
  `PostalCode` varchar(8) COLLATE utf8_polish_ci NOT NULL,
  `CityId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `locations`
--

INSERT INTO `locations` (`Id`, `StreetAddressLine1`, `StreetAddressLine2`, `PostalCode`, `CityId`) VALUES
(1, 'Poniatowskiego', '1', '51-100', 1),
(2, 'Wiejska', '10', '51-100', 1),
(3, 'Haustadt', '10', '51-100', 2),
(4, 'Leibnitz', '12', '51-100', 2);

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `naturalpersonclients`
--

CREATE TABLE `naturalpersonclients` (
  `Id` int(11) UNSIGNED NOT NULL,
  `ForeName` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `LastName` varchar(32) COLLATE utf8_polish_ci NOT NULL,
  `Pesel` varchar(11) COLLATE utf8_polish_ci NOT NULL,
  `NIP` varchar(10) COLLATE utf8_polish_ci DEFAULT NULL,
  `LocationId` int(11) UNSIGNED NOT NULL,
  `PhoneNumber` varchar(15) COLLATE utf8_polish_ci NOT NULL,
  `Mail` varchar(32) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `naturalpersonclients`
--

INSERT INTO `naturalpersonclients` (`Id`, `ForeName`, `LastName`, `Pesel`, `NIP`, `LocationId`, `PhoneNumber`, `Mail`) VALUES
(1, 'Lidia', 'Szmit', '123451234', NULL, 1, '987789662', 'szmitLid@op.pl'),
(2, 'Jhn', 'old', '333451234', NULL, 2, '987789444', 'johnyy@op.pl');

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orders`
--

CREATE TABLE `orders` (
  `Id` int(11) UNSIGNED NOT NULL,
  `IfLegalEntity` bit(1) NOT NULL,
  `ClientId` int(11) UNSIGNED NOT NULL,
  `OrderStateId` int(11) UNSIGNED NOT NULL,
  `DestinationLocationId` int(11) UNSIGNED NOT NULL,
  `SourceLocationId` int(11) UNSIGNED NOT NULL,
  `Description` varchar(255) COLLATE utf8_polish_ci DEFAULT NULL,
  `Deadline` date NOT NULL,
  `Price` double DEFAULT NULL,
  `DepartureDate` date DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `ordersemployees`
--

CREATE TABLE `ordersemployees` (
  `EmployeeId` int(11) UNSIGNED NOT NULL,
  `OrderId` int(11) UNSIGNED NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

-- --------------------------------------------------------

--
-- Struktura tabeli dla tabeli `orderstates`
--

CREATE TABLE `orderstates` (
  `Id` int(11) UNSIGNED NOT NULL,
  `State` varchar(10) COLLATE utf8_polish_ci NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8 COLLATE=utf8_polish_ci;

--
-- Zrzut danych tabeli `orderstates`
--

INSERT INTO `orderstates` (`Id`, `State`) VALUES
(1, 'Accepted'),
(2, 'During'),
(4, 'NODriver'),
(3, 'Realized');

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_allclients_locations`
-- (See below for the actual view)
--
CREATE TABLE `view_allclients_locations` (
`Id` int(11) unsigned
,`ForeName` varchar(32)
,`Company_LastName` varchar(32)
,`Pesel_Regon` varchar(14)
,`NIP` varchar(10)
,`Mail` varchar(32)
,`PhoneNumber` varchar(15)
,`StreetAddressLine1` varchar(32)
,`StreetAddressLine2` varchar(8)
,`PostalCode` varchar(8)
,`CityName` varchar(32)
,`CountryName` varchar(32)
,`IfLegalEntity` varchar(5)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_employees_jobs`
-- (See below for the actual view)
--
CREATE TABLE `view_employees_jobs` (
`Id` int(11) unsigned
,`JobName` varchar(8)
,`FirstName` varchar(32)
,`LastName` varchar(32)
,`Pesel` varchar(11)
,`Mail` varchar(32)
,`PhoneNumber` varchar(16)
,`BirthDate` date
,`MonthSalary` float
,`EmploymentDate` date
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_entitylegalclients_viewlocationscitiescountries`
-- (See below for the actual view)
--
CREATE TABLE `view_entitylegalclients_viewlocationscitiescountries` (
`Id` int(11) unsigned
,`Name` varchar(32)
,`NIP` varchar(10)
,`Regon` varchar(14)
,`Mail` varchar(32)
,`PhoneNumber` varchar(15)
,`StreetAddressLine1` varchar(32)
,`StreetAddressLine2` varchar(8)
,`PostalCode` varchar(8)
,`CityName` varchar(32)
,`CountryName` varchar(32)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_locations_cities_countries`
-- (See below for the actual view)
--
CREATE TABLE `view_locations_cities_countries` (
`Id` int(11) unsigned
,`StreetAddressLine1` varchar(32)
,`StreetAddressLine2` varchar(8)
,`PostalCode` varchar(8)
,`CityName` varchar(32)
,`CountryName` varchar(32)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_naturalpersonclients_viewlocationscitiescountries`
-- (See below for the actual view)
--
CREATE TABLE `view_naturalpersonclients_viewlocationscitiescountries` (
`Id` int(11) unsigned
,`ForeName` varchar(32)
,`LastName` varchar(32)
,`Pesel` varchar(11)
,`NIP` varchar(10)
,`Mail` varchar(32)
,`PhoneNumber` varchar(15)
,`StreetAddressLine1` varchar(32)
,`StreetAddressLine2` varchar(8)
,`PostalCode` varchar(8)
,`CityName` varchar(32)
,`CountryName` varchar(32)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_orders_employees_clients`
-- (See below for the actual view)
--
CREATE TABLE `view_orders_employees_clients` (
`OrderId` int(11) unsigned
,`State` varchar(10)
,`IfLegalEntity` bit(1)
,`ClientId` int(11) unsigned
,`ForeName` varchar(32)
,`Company_LastName` varchar(32)
,`Pesel_Regon` varchar(14)
,`CMail` varchar(32)
,`CPhone` varchar(15)
,`DestinationLocationId` int(11) unsigned
,`SourceLocationId` int(11) unsigned
,`Description` varchar(255)
,`Deadline` date
,`Price` double
,`DepartureDate` date
,`Emp ID` int(11) unsigned
,`FirstName` varchar(32)
,`LastName` varchar(32)
,`Pesel` varchar(11)
,`Mail` varchar(32)
,`PhoneNumber` varchar(16)
);

-- --------------------------------------------------------

--
-- Zastąpiona struktura widoku `view_orders_orderstates_viewlocations`
-- (See below for the actual view)
--
CREATE TABLE `view_orders_orderstates_viewlocations` (
`Id` int(11) unsigned
,`State` varchar(10)
,`IfLegalEntity` bit(1)
,`ClientId` int(11) unsigned
,`Deadline` date
,`DepartureDate` date
,`Description` varchar(255)
,`Price` double
,`Street Dest` varchar(32)
,`Address Dest` varchar(8)
,`Code Dest` varchar(8)
,`City Dest` varchar(32)
,`Country Dest` varchar(32)
,`Street Src` varchar(32)
,`Address Src` varchar(8)
,`Code Src` varchar(8)
,`City Src` varchar(32)
,`Country Src` varchar(32)
);

-- --------------------------------------------------------

--
-- Struktura widoku `view_allclients_locations`
--
DROP TABLE IF EXISTS `view_allclients_locations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_allclients_locations`  AS  select `npc`.`Id` AS `Id`,`npc`.`ForeName` AS `ForeName`,`npc`.`LastName` AS `Company_LastName`,`npc`.`Pesel` AS `Pesel_Regon`,`npc`.`NIP` AS `NIP`,`npc`.`Mail` AS `Mail`,`npc`.`PhoneNumber` AS `PhoneNumber`,`npc`.`StreetAddressLine1` AS `StreetAddressLine1`,`npc`.`StreetAddressLine2` AS `StreetAddressLine2`,`npc`.`PostalCode` AS `PostalCode`,`npc`.`CityName` AS `CityName`,`npc`.`CountryName` AS `CountryName`,'false' AS `IfLegalEntity` from `view_naturalpersonclients_viewlocationscitiescountries` `npc` union select `elc`.`Id` AS `Id`,'' AS `Name_exp_2`,`elc`.`Name` AS `Name`,`elc`.`Regon` AS `Regon`,`elc`.`NIP` AS `NIP`,`elc`.`Mail` AS `Mail`,`elc`.`PhoneNumber` AS `PhoneNumber`,`elc`.`StreetAddressLine1` AS `StreetAddressLine1`,`elc`.`StreetAddressLine2` AS `StreetAddressLine2`,`elc`.`PostalCode` AS `PostalCode`,`elc`.`CityName` AS `CityName`,`elc`.`CountryName` AS `CountryName`,'true' AS `true` from `view_entitylegalclients_viewlocationscitiescountries` `elc` ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_employees_jobs`
--
DROP TABLE IF EXISTS `view_employees_jobs`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_employees_jobs`  AS  select `e`.`Id` AS `Id`,`j`.`JobName` AS `JobName`,`e`.`FirstName` AS `FirstName`,`e`.`LastName` AS `LastName`,`e`.`Pesel` AS `Pesel`,`e`.`Mail` AS `Mail`,`e`.`PhoneNumber` AS `PhoneNumber`,`e`.`BirthDate` AS `BirthDate`,`e`.`MonthSalary` AS `MonthSalary`,`e`.`EmploymentDate` AS `EmploymentDate` from (`employees` `e` join `jobs` `j` on((`e`.`JobsId` = `j`.`Id`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_entitylegalclients_viewlocationscitiescountries`
--
DROP TABLE IF EXISTS `view_entitylegalclients_viewlocationscitiescountries`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_entitylegalclients_viewlocationscitiescountries`  AS  select `en`.`Id` AS `Id`,`en`.`Name` AS `Name`,`en`.`NIP` AS `NIP`,`en`.`Regon` AS `Regon`,`en`.`Mail` AS `Mail`,`en`.`PhoneNumber` AS `PhoneNumber`,`vlcc`.`StreetAddressLine1` AS `StreetAddressLine1`,`vlcc`.`StreetAddressLine2` AS `StreetAddressLine2`,`vlcc`.`PostalCode` AS `PostalCode`,`vlcc`.`CityName` AS `CityName`,`vlcc`.`CountryName` AS `CountryName` from (`entitylegalclients` `en` join `view_locations_cities_countries` `vlcc` on((`en`.`Id` = `vlcc`.`Id`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_locations_cities_countries`
--
DROP TABLE IF EXISTS `view_locations_cities_countries`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_locations_cities_countries`  AS  select `l`.`Id` AS `Id`,`l`.`StreetAddressLine1` AS `StreetAddressLine1`,`l`.`StreetAddressLine2` AS `StreetAddressLine2`,`l`.`PostalCode` AS `PostalCode`,`ci`.`CityName` AS `CityName`,`co`.`CountryName` AS `CountryName` from ((`cities` `ci` join `countries` `co` on((`ci`.`CountryId` = `co`.`Id`))) join `locations` `l` on((`ci`.`Id` = `l`.`CityId`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_naturalpersonclients_viewlocationscitiescountries`
--
DROP TABLE IF EXISTS `view_naturalpersonclients_viewlocationscitiescountries`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_naturalpersonclients_viewlocationscitiescountries`  AS  select `na`.`Id` AS `Id`,`na`.`ForeName` AS `ForeName`,`na`.`LastName` AS `LastName`,`na`.`Pesel` AS `Pesel`,`na`.`NIP` AS `NIP`,`na`.`Mail` AS `Mail`,`na`.`PhoneNumber` AS `PhoneNumber`,`vlcc`.`StreetAddressLine1` AS `StreetAddressLine1`,`vlcc`.`StreetAddressLine2` AS `StreetAddressLine2`,`vlcc`.`PostalCode` AS `PostalCode`,`vlcc`.`CityName` AS `CityName`,`vlcc`.`CountryName` AS `CountryName` from (`naturalpersonclients` `na` join `view_locations_cities_countries` `vlcc` on((`na`.`Id` = `vlcc`.`Id`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_orders_employees_clients`
--
DROP TABLE IF EXISTS `view_orders_employees_clients`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_orders_employees_clients`  AS  select `o`.`Id` AS `OrderId`,`os`.`State` AS `State`,`o`.`IfLegalEntity` AS `IfLegalEntity`,`v`.`Id` AS `ClientId`,`v`.`ForeName` AS `ForeName`,`v`.`Company_LastName` AS `Company_LastName`,`v`.`Pesel_Regon` AS `Pesel_Regon`,`v`.`Mail` AS `CMail`,`v`.`PhoneNumber` AS `CPhone`,`o`.`DestinationLocationId` AS `DestinationLocationId`,`o`.`SourceLocationId` AS `SourceLocationId`,`o`.`Description` AS `Description`,`o`.`Deadline` AS `Deadline`,`o`.`Price` AS `Price`,`o`.`DepartureDate` AS `DepartureDate`,`e`.`Id` AS `Emp ID`,`e`.`FirstName` AS `FirstName`,`e`.`LastName` AS `LastName`,`e`.`Pesel` AS `Pesel`,`e`.`Mail` AS `Mail`,`e`.`PhoneNumber` AS `PhoneNumber` from ((((`orders` `o` left join `ordersemployees` `eo` on((`o`.`Id` = `eo`.`OrderId`))) join `employees` `e` on((`eo`.`EmployeeId` = `e`.`Id`))) join `view_allclients_locations` `v` on(((`o`.`ClientId` = `v`.`Id`) and (`o`.`IfLegalEntity` = `v`.`IfLegalEntity`)))) join `orderstates` `os` on((`o`.`OrderStateId` = `os`.`Id`))) ;

-- --------------------------------------------------------

--
-- Struktura widoku `view_orders_orderstates_viewlocations`
--
DROP TABLE IF EXISTS `view_orders_orderstates_viewlocations`;

CREATE ALGORITHM=UNDEFINED DEFINER=`root`@`localhost` SQL SECURITY DEFINER VIEW `view_orders_orderstates_viewlocations`  AS  select `ord`.`Id` AS `Id`,`ors`.`State` AS `State`,`ord`.`IfLegalEntity` AS `IfLegalEntity`,`ord`.`ClientId` AS `ClientId`,`ord`.`Deadline` AS `Deadline`,`ord`.`DepartureDate` AS `DepartureDate`,`ord`.`Description` AS `Description`,`ord`.`Price` AS `Price`,`vlcc1`.`StreetAddressLine1` AS `Street Dest`,`vlcc1`.`StreetAddressLine2` AS `Address Dest`,`vlcc1`.`PostalCode` AS `Code Dest`,`vlcc1`.`CityName` AS `City Dest`,`vlcc1`.`CountryName` AS `Country Dest`,`vlcc2`.`StreetAddressLine1` AS `Street Src`,`vlcc2`.`StreetAddressLine2` AS `Address Src`,`vlcc2`.`PostalCode` AS `Code Src`,`vlcc2`.`CityName` AS `City Src`,`vlcc2`.`CountryName` AS `Country Src` from (((`orders` `ord` join `orderstates` `ors` on((`ord`.`OrderStateId` = `ors`.`Id`))) join `view_locations_cities_countries` `vlcc1` on((`ord`.`DestinationLocationId` = `vlcc1`.`Id`))) join `view_locations_cities_countries` `vlcc2` on((`ord`.`SourceLocationId` = `vlcc2`.`Id`))) ;

--
-- Indeksy dla zrzutów tabel
--

--
-- Indexes for table `cities`
--
ALTER TABLE `cities`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `CityName` (`CityName`),
  ADD KEY `CountryId` (`CountryId`),
  ADD KEY `CountryId_2` (`CountryId`);

--
-- Indexes for table `countries`
--
ALTER TABLE `countries`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`CountryName`),
  ADD KEY `Id` (`Id`),
  ADD KEY `Id_2` (`Id`);

--
-- Indexes for table `employees`
--
ALTER TABLE `employees`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`Pesel`),
  ADD KEY `LastName` (`LastName`),
  ADD KEY `Mail` (`Mail`),
  ADD KEY `FK_Employees_EmployeeJobs` (`JobsId`);

--
-- Indexes for table `entitylegalclients`
--
ALTER TABLE `entitylegalclients`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`Regon`),
  ADD KEY `Name` (`Name`);

--
-- Indexes for table `jobs`
--
ALTER TABLE `jobs`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`JobName`);

--
-- Indexes for table `locations`
--
ALTER TABLE `locations`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `FK_Location_Cities` (`CityId`);

--
-- Indexes for table `naturalpersonclients`
--
ALTER TABLE `naturalpersonclients`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`Pesel`),
  ADD KEY `LastName` (`LastName`),
  ADD KEY `FK_NaturalPersonClients_Locations` (`LocationId`);

--
-- Indexes for table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`Id`),
  ADD KEY `Deadline` (`Deadline`),
  ADD KEY `DepartureDate` (`DepartureDate`),
  ADD KEY `FK_Orders_Locations` (`DestinationLocationId`),
  ADD KEY `FK_Orders_Locations1` (`SourceLocationId`),
  ADD KEY `FK_Orders_NaturalPersonClients` (`ClientId`),
  ADD KEY `FK_Orders_OrderStates` (`OrderStateId`);

--
-- Indexes for table `ordersemployees`
--
ALTER TABLE `ordersemployees`
  ADD PRIMARY KEY (`EmployeeId`,`OrderId`),
  ADD KEY `FK_OrdersEmployees_Orders` (`OrderId`);

--
-- Indexes for table `orderstates`
--
ALTER TABLE `orderstates`
  ADD PRIMARY KEY (`Id`),
  ADD UNIQUE KEY `NONCLUSTERED` (`State`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT dla tabeli `cities`
--
ALTER TABLE `cities`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `countries`
--
ALTER TABLE `countries`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `employees`
--
ALTER TABLE `employees`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;

--
-- AUTO_INCREMENT dla tabeli `entitylegalclients`
--
ALTER TABLE `entitylegalclients`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `jobs`
--
ALTER TABLE `jobs`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT dla tabeli `locations`
--
ALTER TABLE `locations`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- AUTO_INCREMENT dla tabeli `naturalpersonclients`
--
ALTER TABLE `naturalpersonclients`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=3;

--
-- AUTO_INCREMENT dla tabeli `orders`
--
ALTER TABLE `orders`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT;

--
-- AUTO_INCREMENT dla tabeli `orderstates`
--
ALTER TABLE `orderstates`
  MODIFY `Id` int(11) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;

--
-- Ograniczenia dla zrzutów tabel
--

--
-- Ograniczenia dla tabeli `cities`
--
ALTER TABLE `cities`
  ADD CONSTRAINT `FK_Cities_Countries` FOREIGN KEY (`CountryId`) REFERENCES `countries` (`Id`);

--
-- Ograniczenia dla tabeli `employees`
--
ALTER TABLE `employees`
  ADD CONSTRAINT `FK_Employees_EmployeeJobs` FOREIGN KEY (`JobsId`) REFERENCES `jobs` (`Id`);

--
-- Ograniczenia dla tabeli `locations`
--
ALTER TABLE `locations`
  ADD CONSTRAINT `FK_Location_Cities` FOREIGN KEY (`CityId`) REFERENCES `cities` (`Id`);

--
-- Ograniczenia dla tabeli `naturalpersonclients`
--
ALTER TABLE `naturalpersonclients`
  ADD CONSTRAINT `FK_NaturalPersonClients_Locations` FOREIGN KEY (`LocationId`) REFERENCES `locations` (`Id`);

--
-- Ograniczenia dla tabeli `orders`
--
ALTER TABLE `orders`
  ADD CONSTRAINT `FK_Orders_EntityLegalClients` FOREIGN KEY (`ClientId`) REFERENCES `entitylegalclients` (`Id`),
  ADD CONSTRAINT `FK_Orders_Locations` FOREIGN KEY (`DestinationLocationId`) REFERENCES `locations` (`Id`),
  ADD CONSTRAINT `FK_Orders_Locations1` FOREIGN KEY (`SourceLocationId`) REFERENCES `locations` (`Id`),
  ADD CONSTRAINT `FK_Orders_NaturalPersonClients` FOREIGN KEY (`ClientId`) REFERENCES `naturalpersonclients` (`Id`),
  ADD CONSTRAINT `FK_Orders_OrderStates` FOREIGN KEY (`OrderStateId`) REFERENCES `orderstates` (`Id`);

--
-- Ograniczenia dla tabeli `ordersemployees`
--
ALTER TABLE `ordersemployees`
  ADD CONSTRAINT `FK_OrdersEmployees_Employees` FOREIGN KEY (`EmployeeId`) REFERENCES `employees` (`Id`),
  ADD CONSTRAINT `FK_OrdersEmployees_Orders` FOREIGN KEY (`OrderId`) REFERENCES `orders` (`Id`);
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
